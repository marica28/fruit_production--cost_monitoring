package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import Controllers.WorkerCtrl.WorkerListener;
import Data.Worker;
import Data.TypeOfPayment;
import Model.Model;
import View.frmTypeOfPayment;

public class TypeOfPaymentCtrl implements Observer {

	private String [] header = {"ID","Name"};
	private frmTypeOfPayment view;
	private Model model;
	
	public TypeOfPaymentCtrl(frmTypeOfPayment view, Model model) {
		this.view=view;
		this.model=model;
		view.setSaveListener(new TypeOfPaymentListener());;
		model.addObserver(this);
		this.model.getAllPayment();
	}

	@Override
	public void update(Observable o, Object arg) 
	{
		ArrayList<TypeOfPayment> list = (ArrayList<TypeOfPayment>) arg;
		Object[][] data = new Object[list.size()][header.length];
		int i = 0;
		for (TypeOfPayment row : list)
			{
				data[i][0] = row.getType_of_payment_id();
				data[i][1] = row.getName();
				i++;
			}
		view.setTableData(header, data);
	}
	
public class TypeOfPaymentListener implements ActionListener
	{

	@Override
	public void actionPerformed(ActionEvent arg0) {
		TypeOfPayment pay = new TypeOfPayment(0, view.getTypeP());
		model.SavePayment(pay);
		model.getAllPayment();
		view.Clear();
		}
	}

}

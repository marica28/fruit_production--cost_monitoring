package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import Data.ActivityRecord;
import Data.Machine;
import Data.MachineItem;
import Model.Model;
import View.frmActivityRecord;
import View.frmMain;


public class MainCtrl implements Observer{

	private Model model;
	private String [] header = {"ID","Date","Name","Description","Location"};
	private frmMain view;
	private MachineItem machine;
	private ActivityRecord activity;
	private frmActivityRecord record;
	
	public MainCtrl(frmMain view, Model model) {
		this.view=view;
		this.model=model;
		model.addObserver(this);
		model.getAllActivities();
		view.EditActivity(new EditListener("editAct"));
		view.setSearchListenerDate(new EditListener("searchDate"));
		view.setSearchListenerName(new EditListener("searchName"));
		view.setSearchListenerLoc(new EditListener("searchLoc"));
		view.setEndSearchDate(new EditListener("endSearch"));
		view.setEndSearchName(new EditListener("endSearch"));
		view.setEndSearchLoc(new EditListener("endSearch"));
	}

	@Override
	public void update(Observable arg0, Object arg1) {
			ArrayList<ActivityRecord>list = (ArrayList<ActivityRecord>)arg1;
			Object [][] data = new Object[list.size()][header.length];
			int i=0;
			for(ActivityRecord row:list) {
				data[i][0] = row.getAct_id();
				data[i][1] = getDate(row.getDateAct());
				data[i][2] = row;
				data[i][3] = row.getDescriptionAct();
				data[i][4] = row.getLocationAct();
				i++;
			}
			view.setTableData(header, data);
	}

	public class EditListener implements ActionListener{
		private String edit;

		public EditListener(String edit) {
			this.edit = edit;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
		if(edit.equals("editAct")) {
			frmActivityRecord akt = new frmActivityRecord("Izmena aktivnosti");
			Model mod = new Model();
			ActivityCtrl aktkon=new ActivityCtrl(akt, mod, view.getSelectedActivity(), null, null, null);
			akt.setVisible(true);
			akt.setDateAct(getDate(view.getSelectedActivity().getDateAct()));
			akt.setNameAct(view.getSelectedActivity().getNameAct());
			akt.setDescriptionAct(view.getSelectedActivity().getDescriptionAct());
			akt.setLocationAct(view.getSelectedActivity().getLocationAct());
			akt.setFinishDisable();
		} else if(edit.equals("searchDate")) {
			model.getActivityDate(view.getDateFrom(), view.getDateTo());
		} else if(edit.equals("searchName")) {
			model.getActivityName(view.getNameAct());
		} else if(edit.equals("searchLoc")) {
			model.getActivityLocation(view.getLocaticon());
		} else if(edit.equals("endSearch")) {
			model.getAllActivities();
		}
		}
	}
	public String getDate(long timeStamp)
	{
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		Date time=new Date((long)timeStamp*1000);
		return df.format(time);
	}

	
}

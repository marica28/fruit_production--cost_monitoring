package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import Data.Worker;
import Data.TypeOfPayment;
import Model.Model;
import View.frmWorker;

public class WorkerCtrl implements Observer{

	private String [] header = {"ID","Name","Phone Number", "Type of Payment"};
	private frmWorker view;
	private Model model;
	
	public WorkerCtrl(frmWorker view, Model model) {
		this.view=view;
		this.model=model;
		view.setSaveListener(new WorkerListener());
		model.addObserver(this);
		this.model.getAllWorker();
		this.model.getAllPayment();
	}

	@Override
	public void update(Observable o, Object arg) 
	{
		if(!((ArrayList<Object>)arg).isEmpty()) {
			
		if(((ArrayList<Object>)arg).get(0) instanceof Worker)
		{
			ArrayList<Worker> list = (ArrayList<Worker>) arg;
			Object[][] data = new Object[list.size()][header.length];
			int i = 0;
			for (Worker red : list)
				{
					data[i][0] = red.getWorker_id();
					data[i][1] = red.getNameWor();
					data[i][2] = red.getPhone_number();
					data[i][3] = red.getPayment();
					i++;
				}
			view.setTableData(header, data);
		}
		else if (((ArrayList<Object>)arg).get(0) instanceof TypeOfPayment)
		{
			ArrayList<TypeOfPayment>type = (ArrayList<TypeOfPayment>)arg;
			view.setTypePaymentCB(type);
		}
		}
	}
public class WorkerListener implements ActionListener
	{

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Worker wor = new Worker(0,view.getNameWorker(),view.getPhoneNumber(),(TypeOfPayment) view.getTypePayment());
		model.SaveWorker(wor);;
		model.getAllWorker();
		view.ClearF();
		}
	}

}

package Controllers;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFileChooser;

import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import Data.MachineItem;
import Data.MaterialItem;
import Data.Worker;
import Data.WorkerItem;
import Model.Model;
import View.frmReports;


public class ReportsCtrl implements Observer{

	private Model model;
	private frmReports view;
	private ArrayList<MachineItem> machineItem;
	private ArrayList<MaterialItem> materialItem;
	private ArrayList<WorkerItem> workerItem;
	private String [] header = {"Machine", "Total Work Time","Total Cost"};
	private String [] header1 = {"Material","Total Quantity","Total Cost"};
	private String [] header2 = {"Worker","Total Work Time","Total Cost"};
	
	
	public ReportsCtrl(Model model, frmReports view) {
		this.model = model;
		this.view = view;
		model.addObserver(this);
		this.model.getAllMachineReport();;
		this.model.getAllMaterialReport();
		this.model.getAllWorkerReport();;
		view.PDFMachine(new ReportListener("machine"));
		view.PDFMaterial(new ReportListener("material"));
		view.PDFWorker(new ReportListener("worker"));
	}

	@Override
	public void update(Observable o, Object arg) {
	
		if (((ArrayList<Object>) arg).get(0) instanceof MachineItem) 
		{
			machineItem = (ArrayList<MachineItem>)arg;
			Object [][] data = new Object[machineItem.size()][header.length];
			int i=0;
			for(MachineItem row:machineItem)
			{
				data[i][0] = row.getMachine();
				data[i][1] = row.getWork_time_machine();
				data[i][2] = row.getLabor_cost_machine();
				i++;
			}
			double sum=0;
			for(MachineItem mac:machineItem) {
				sum=sum+mac.getLabor_cost_machine();
			}
			view.setTableDataMas(header, data);
			view.setSumMac(sum);
		}
		if (((ArrayList<Object>) arg).get(0) instanceof MaterialItem)
		{
			materialItem = (ArrayList<MaterialItem>)arg;
			Object[][] data1 = new Object[materialItem.size()][header.length];
			int i=0;
			for(MaterialItem row:materialItem) 
			{
				data1[i][0] = row.getMaterial();
				data1[i][1] = row.getQuantity_material();
				data1[i][2] = row.getCost_material();
				i++;
			}
			double sum=0;
			for(MaterialItem mat:materialItem) {
				sum=sum+mat.getCost_material();
			}
			view.setTableDataMat(header1, data1);
			view.setSumMat(sum);
		}
		if (((ArrayList<Object>) arg).get(0) instanceof WorkerItem) 
		{ 
			workerItem = (ArrayList<WorkerItem>)arg;
			Object [][] data2 = new Object[workerItem.size()][header.length];
			int i=0;
			for (WorkerItem row:workerItem)
			{
				data2[i][0] = row.getWorker();
				data2[i][1] = row.getWork_time_worker();
				data2[i][2] = row.getCost();
				i++;
			}
			double sum=0;
			for(WorkerItem w:workerItem) {
				sum=sum+w.getCost();
			}
			view.setTableDataRad(header2, data2);
			view.setSumWor(sum);
		}
	}

	public void pdfMasine() {
		
		JFileChooser c = new JFileChooser();
		String path = "";
	      // Demonstrate "Open" dialog:
	      int rVal = c.showSaveDialog(view);
	      if (rVal == JFileChooser.APPROVE_OPTION) {
	    	  path = c.getSelectedFile().getAbsolutePath();
		
	    Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path)); 
			
			document.open(); 
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
			Date date = new Date();
			
			
			PdfPTable table1 = new PdfPTable(3);
			PdfPCell datePdf = new PdfPCell(new Paragraph("Datum: "+ dateFormat.format(date), new Font(Font.FontFamily.TIMES_ROMAN,18))); 
			datePdf.setColspan(3); 
			datePdf.setBorder(0); 
			datePdf.setHorizontalAlignment(Element.ALIGN_LEFT); 
			table1.addCell(datePdf); 
			PdfPCell gazd = new PdfPCell(new Paragraph("Gazdinstvo: ",new Font(Font.FontFamily.TIMES_ROMAN,18)));
			gazd.setColspan(3);
			gazd.setBorder(0);
			gazd.setHorizontalAlignment(Element.ALIGN_LEFT);
			table1.addCell(gazd);
			PdfPCell eeee = new PdfPCell(new Paragraph(" ")); 
			eeee.setColspan(3);
			eeee.setBorder(0);
			table1.addCell(eeee); 
			
			PdfPCell title = new PdfPCell(new Paragraph("Izvestaj troskova masine",  new Font(Font.FontFamily.TIMES_ROMAN,18,Font.BOLD))); 
			title.setColspan(3);
			title.setBorder(0);
			title.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(title); 
			document.add(table1); 
			Paragraph empty = new Paragraph(" "); 
			addEmptyLine(empty, 2); 
			document.add(empty); 
			
			Font tblFont = new Font(new Font(Font.FontFamily.TIMES_ROMAN,15)); 
			Font hedFont = new Font(new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD)); 
			
			PdfPTable table = new PdfPTable(3);		
			PdfPCell h1 = new PdfPCell(new Paragraph("Masina",hedFont));
			h1.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(h1);
			PdfPCell h2 = new PdfPCell(new Paragraph("Ukupno vreme rada", hedFont));
			h2.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(h2);
			PdfPCell h3 = new PdfPCell(new Paragraph("Ukupna cena rada", hedFont));
			h3.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(h3); 
			for(MachineItem mac:machineItem) { 
				PdfPCell cell1 = new PdfPCell(new Paragraph(mac.getMachine().getNameMac(),tblFont));
				table.addCell(cell1);
				PdfPCell cell2 = new PdfPCell(new Paragraph(String.valueOf(mac.getWork_time_machine()), tblFont));
				table.addCell(cell2);
				PdfPCell cell3 = new PdfPCell(new Paragraph(String.valueOf(mac.getLabor_cost_machine()), tblFont));
				table.addCell(cell3);
			}
			
			document.add(table); 
			document.close(); 
		} catch(Exception e) {
			e.printStackTrace();
		}
	  }
	}
	
	public void pdfMaterijali() {
		
		JFileChooser c = new JFileChooser();
		String path = "";
	      // Demonstrate "Open" dialog:
		int rVal = c.showSaveDialog(view);
	    if (rVal == JFileChooser.APPROVE_OPTION) {
	   	  path = c.getSelectedFile().getAbsolutePath();
	    	  
	   	  Document document = new Document();
	   	  try {
		   		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
		   		document.open();
		   	
		   		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
		   	 	Date date = new Date();
					
		   		PdfPTable table1 = new PdfPTable(3);
		   		PdfPCell datePdf = new PdfPCell(new Paragraph("Datum: "+ dateFormat.format(date),  new Font(Font.FontFamily.TIMES_ROMAN,18))); 
		   		datePdf.setColspan(3);
				datePdf.setBorder(0);
				datePdf.setHorizontalAlignment(Element.ALIGN_LEFT);
				table1.addCell(datePdf);
				PdfPCell gazd = new PdfPCell(new Paragraph("Gazdinstvo: ",new Font(Font.FontFamily.TIMES_ROMAN,18)));
				gazd.setColspan(3);
				gazd.setBorder(0);
				gazd.setHorizontalAlignment(Element.ALIGN_LEFT);
				table1.addCell(gazd);
				PdfPCell eeee = new PdfPCell(new Paragraph(" "));
				eeee.setColspan(3);
				eeee.setBorder(0);
				table1.addCell(eeee);
					
				PdfPCell title = new PdfPCell(new Paragraph("Izvestaj troskova materijala",  new Font(Font.FontFamily.TIMES_ROMAN,18,Font.BOLD))); 
				title.setColspan(3);
				title.setBorder(0);
				title.setHorizontalAlignment(Element.ALIGN_CENTER);
				table1.addCell(title);
				document.add(table1);
				Paragraph empty = new Paragraph(" ");
				addEmptyLine(empty, 2);
				document.add(empty);
					
				Font tblFont = new Font(new Font(Font.FontFamily.TIMES_ROMAN,15));
				Font hedFont = new Font(new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD));
				PdfPTable table = new PdfPTable(3);
				PdfPCell h1 = new PdfPCell(new Paragraph("Materijal",hedFont));
				h1.setBackgroundColor(BaseColor.LIGHT_GRAY);
				table.addCell(h1);
				PdfPCell h2 = new PdfPCell(new Paragraph("Ukupna kolicina materijala", hedFont));
				h2.setBackgroundColor(BaseColor.LIGHT_GRAY);
				table.addCell(h2);
				PdfPCell h3 = new PdfPCell(new Paragraph("Ukupna cena materijala", hedFont));
				h3.setBackgroundColor(BaseColor.LIGHT_GRAY);
				table.addCell(h3);
				for(MaterialItem mat:materialItem) {
					PdfPCell cell1 = new PdfPCell(new Paragraph(String.valueOf(mat.getMaterial()),tblFont));
					table.addCell(cell1);
					PdfPCell cell2 = new PdfPCell(new Paragraph(String.valueOf(mat.getQuantity_material()),tblFont));
					table.addCell(cell2);
					PdfPCell cell3 = new PdfPCell(new Paragraph(String.valueOf(mat.getCost_material()), tblFont));
					table.addCell(cell3);
				}
					
				document.add(table);
				document.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		  }
	}
	
	public void pdfRadnici() {
		JFileChooser c = new JFileChooser();
		String path = "";
	      // Demonstrate "Open" dialog:
	      int rVal = c.showSaveDialog(view);
	      if (rVal == JFileChooser.APPROVE_OPTION) 
	      {
	    	  path = c.getSelectedFile().getAbsolutePath();
	    	  
	    	  Document document = new Document();
	    	  try {
					PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
					document.open();
					
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					Date date = new Date();
					
					PdfPTable table1 = new PdfPTable(3);
					PdfPCell datePdf = new PdfPCell(new Paragraph("Datum: "+ dateFormat.format(date),  new Font(Font.FontFamily.TIMES_ROMAN,18))); 
					datePdf.setColspan(3);
					datePdf.setBorder(0);
					datePdf.setHorizontalAlignment(Element.ALIGN_LEFT);
					table1.addCell(datePdf);
					PdfPCell gazd = new PdfPCell(new Paragraph("Gazdinstvo: ",new Font(Font.FontFamily.TIMES_ROMAN,18)));
					gazd.setColspan(3);
					gazd.setBorder(0);
					gazd.setHorizontalAlignment(Element.ALIGN_LEFT);
					table1.addCell(gazd);
					PdfPCell eeee = new PdfPCell(new Paragraph(" ")); 
					eeee.setColspan(3);
					eeee.setBorder(0);
					table1.addCell(eeee);
					
					PdfPCell title = new PdfPCell(new Paragraph("Izvestaj troskova radnika",  new Font(Font.FontFamily.TIMES_ROMAN,18,Font.BOLD))); 
					title.setColspan(3);
					title.setBorder(0);
					title.setHorizontalAlignment(Element.ALIGN_CENTER);
					table1.addCell(title);
					document.add(table1);
					Paragraph empty = new Paragraph(" ");
					addEmptyLine(empty, 2);	
					document.add(empty);
					Font tblFont = new Font(new Font(Font.FontFamily.TIMES_ROMAN,15));
					Font hedFont = new Font(new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD));
					PdfPTable table = new PdfPTable(3);
					PdfPCell h1 = new PdfPCell(new Paragraph("Radnik",hedFont));
					h1.setBackgroundColor(BaseColor.LIGHT_GRAY);
					table.addCell(h1);
					PdfPCell h2 = new PdfPCell(new Paragraph("Ukupno vreme rada", hedFont));
					h2.setBackgroundColor(BaseColor.LIGHT_GRAY);
					table.addCell(h2);
					PdfPCell h3 = new PdfPCell(new Paragraph("Ukupna cena rada", hedFont));
					h3.setBackgroundColor(BaseColor.LIGHT_GRAY);
					table.addCell(h3);
					for(WorkerItem wor:workerItem) {
						PdfPCell cell1 = new PdfPCell(new Paragraph(String.valueOf(wor.getWorker()),tblFont));
						table.addCell(cell1);
						PdfPCell cell2 = new PdfPCell(new Paragraph(String.valueOf(wor.getWork_time_worker()),tblFont));
						table.addCell(cell2);
						PdfPCell cell3 = new PdfPCell(new Paragraph(String.valueOf(wor.getCost()), tblFont));
						table.addCell(cell3);
					}
					
					document.add(table);
					document.close();
	    	  } catch(Exception e) {
	    		  e.printStackTrace();
	    	  }
	      } 		  
	}
	private void addEmptyLine(Paragraph paragraph, int number) {
    for (int i = 0; i < number; i++) {
        paragraph.add(new Paragraph(" "));
    	}
	}
	
	public class ReportListener implements ActionListener {
		private String action;
		
		public ReportListener(String action) {
			this.action=action;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			if(action.equals("machine")) {
				pdfMasine();
			}
			if(action.equals("material")) {
				pdfMaterijali();
			}
			if(action.equals("worker")) {
				pdfRadnici();
			}
			
		}
		
	}

}

package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import Data.Material;
import Model.Model;
import View.frmMaterial;

public class MaterialCtrl implements Observer {

	private String [] header = {"ID","Name ","Unit Measure"};
	private frmMaterial view;
	private Model model;
	
	public MaterialCtrl(frmMaterial view, Model model) {
 
		this.view=view;
		this.model=model;
		view.setSaveListener(new MaterialListener());
		model.addObserver(this);
		this.model.getAllMaterial();
	}

	@Override
	public void update(Observable o, Object arg) {
		
		ArrayList<Material> list = (ArrayList<Material>)arg;
		Object[][] data = new Object[list.size()][header.length];
		int i=0;
		for(Material row:list) {
			data[i][0] = row.getMaterial_id();
			data[i][1] = row.getNameMat();
			data[i][2] = row.getUnit_measure();
			i++;
		}
		view.setTableData(header, data);
	}

public class MaterialListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		Material mat = new Material(0, view.getName(), view.getUnitM());
		model.SaveMaterial(mat);
		model.getAllMaterial();
		view.ClearF();
	}
	
}

}

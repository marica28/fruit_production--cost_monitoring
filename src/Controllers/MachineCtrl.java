package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import Data.Machine;
import Data.Material;
import Model.Model;
import View.frmMachine;

public class MachineCtrl implements Observer {

	private String [] header = {"ID","Name"};
	private frmMachine view;
	private Model model;
	
	public MachineCtrl(frmMachine view, Model model) {
		this.view=view;
		this.model=model;
		view.setSaveListener(new MachineListener());
		model.addObserver(this);
		this.model.getAllMachine();
	}

	@Override
	public void update(Observable o, Object arg) {
		
		ArrayList<Machine> list = (ArrayList<Machine>)arg;
		Object[][] data = new Object[list.size()][header.length];
		int i=0;
		for(Machine row:list) {
			data[i][0] = row.getMachine_id();
			data[i][1] = row.getNameMac();
			i++;
		}
		view.setTableData(header, data);
	}
public class MachineListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		Machine mach = new Machine(0, view.getName());
		model.SaveMachine(mach);
		model.getAllMachine();
		view.ClearF();
	}
	
}
}

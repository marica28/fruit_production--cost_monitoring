package Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import Data.ActivityRecord;
import Data.Machine;
import Data.MachineItem;
import Data.Material;
import Data.MaterialItem;
import Data.Worker;
import Data.WorkerItem;
import Model.Model;
import View.frmActivityRecord;


public class ActivityCtrl implements Observer {

	private String[] header = { "ID", "Machine", "Work Time", "Cost", "Activity", "TOTAL" };
	private String[] header2 = { "ID", "Material", "Quantity", "Cost", "Activity","TOTAL" };
	private String[] header3 = { "ID", "Worker", "Work Time","Cost", "Activity","TOTAL" };
	private frmActivityRecord view;
	private Model model;
	private ActivityRecord activity;
	private MachineItem machine;
	private MaterialItem material;
	private WorkerItem worker;
	private double sumaR;
	private double sumaM;
	private double sumaMa;

	public ActivityCtrl(frmActivityRecord view, Model model) {
		this.view = view;
		this.model = model;
		view.setSaveListener(new activityListener("activity"));
		view.setAddListener(new editListener ("machine"));
		view.setAddListener2(new editListener("material"));
		view.setAddListener3(new editListener("worker"));
		view.setFinishListener(new activityListener("finish"));
		model.addObserver(this);
		this.model.getAllMachine();
		this.model.getAllMaterial();
		this.model.getAllWorker();
		
	}

	public ActivityCtrl(frmActivityRecord view, Model model, ActivityRecord activity, MachineItem machine, MaterialItem material, WorkerItem worker) {
		this.view = view;
		this.model = model;
		this.activity = activity;
		this.machine = machine;
		this.material = material;
		this.worker = worker;
		model.addObserver(this);
		this.model.getAllWorkerItems();
		this.model.getAllMachineItems();
		this.model.getAllMaterialItems();
		this.model.getAllWorker();
		this.model.getAllMachine();
		this.model.getAllMaterial();
		view.setSaveListener(new activityListener("edit"));
		view.setEditActivity("edit");
		view.setAddListener(new editListener("machine"));
		view.setAddListener2(new editListener("material"));
		view.setAddListener3(new editListener("worker"));
		view.EditMachine(new editListener("machineEdit"));
		view.EditMaterial(new editListener("materialEdit"));
		view.EditWorker(new editListener("workerEdit"));
		
	}

	@Override
	public void update(Observable o, Object arg) {

		if (!((ArrayList<Object>) arg).isEmpty()) {
			
			if (((ArrayList<Object>) arg).get(0) instanceof MachineItem) {
				sumaM=0;
				ArrayList<MachineItem> list = (ArrayList<MachineItem>) arg;
				Object[][] data = new Object[(int) list.stream().filter(x->x.getActivity().getAct_id() ==activity.getAct_id()).count()][header.length];
				int i = 0;
				for (MachineItem mas : list) {
					if ((mas.getActivity().getAct_id()) == (activity.getAct_id())) {
						data[i][0] = mas.getMachine_item_id();
						data[i][1] = mas;
						data[i][2] = mas.getWork_time_machine();
						data[i][3] = mas.getLabor_cost_machine();
						data[i][4] = mas.getActivity().getAct_id();
						data[i][5] = mas.getLabor_cost_machine()*mas.getWork_time_machine();
						sumaM +=mas.getLabor_cost_machine()*mas.getWork_time_machine();
						i++;
					}
				}
				view.setTableMachine(header, data);
			} else if (((ArrayList<Object>) arg).get(0) instanceof MaterialItem) {
				sumaMa=0;
				ArrayList<MaterialItem> list = (ArrayList<MaterialItem>) arg;
				Object[][] data2 = new Object[(int) list.stream().filter(m-> m.getActivity().getAct_id()==activity.getAct_id()).count()][header2.length];
				int i = 0;
				for (MaterialItem mat : list) {
					if ((mat.getActivity().getAct_id()) == (activity.getAct_id())) {
						data2[i][0] = mat.getMaterial_item_id();
						data2[i][1] = mat;
						data2[i][2] = mat.getQuantity_material();
						data2[i][3] = mat.getCost_material();
						data2[i][4] = mat.getActivity().getAct_id();
						data2[i][5] = mat.getCost_material()*mat.getQuantity_material();
						sumaMa +=mat.getCost_material()*mat.getQuantity_material();
						i++;
					}
				}
				view.setTableMaterial(header2, data2);
			} else if (((ArrayList<Object>) arg).get(0) instanceof WorkerItem) {
				sumaR=0;
				ArrayList<WorkerItem> list = (ArrayList<WorkerItem>) arg;
				Object[][] data3 = new Object[(int)list.stream().filter(r->r.getActivity().getAct_id()==activity.getAct_id()).count()][header3.length];
				int i = 0;
				for (WorkerItem wor : list) {
					if ((wor.getActivity().getAct_id()) == (activity.getAct_id())) {
						data3[i][0] = wor.getWorker_item_id();
						data3[i][1] = wor;
						data3[i][2] = wor.getWork_time_worker();
						data3[i][3] = wor.getCost();
						data3[i][4] = wor.getActivity().getAct_id();
						data3[i][5] = wor.getCost()*wor.getWork_time_worker();
						sumaR +=wor.getCost()*wor.getWork_time_worker();
						i++;
					}
				}
				view.setTableWorker(header3, data3);
			} else if (((ArrayList<Object>) arg).get(0) instanceof Machine) {
				ArrayList<Machine> machine = (ArrayList<Machine>) arg;
				view.setCBMachine(machine);
			} else if (((ArrayList<Object>) arg).get(0) instanceof Material) {
				ArrayList<Material> material = (ArrayList<Material>) arg;
				view.setCBMaterial(material);
			} else if (((ArrayList<Object>) arg).get(0) instanceof Worker) {
				ArrayList<Worker> worker = (ArrayList<Worker>) arg;
				view.setCBWorker(worker);
			}
			view.setSUMActivity(sumaM+sumaMa+sumaR);
		}
	}

	public class activityListener implements ActionListener {

		private String action;

		public activityListener(String action) {
			this.action = action;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (action.equals("activity")) {
				activity = new ActivityRecord(0, view.getDateAct(), view.getNameAct(), view.getDescriptionAct(),
						view.getLocationAct());
				model.SaveActivity(activity);
			} else if (action.equals("zavrsi")) {
				view.ClearFields();
				view.ClearTables();
				view.setSUMActivity(sumaM=sumaR=sumaMa=0.0);
			} else if (action.equals("edit")) {
				activity.setDateAct(view.getDateAct());
				activity.setNameAct(view.getNameAct());
				activity.setLocationAct(view.getLocationAct());
				activity.setDescriptionAct(view.getDescriptionAct());
				model.EditActivity(activity);
				model.getAllWorkerItems();
				model.getAllMachineItems();
				model.getAllMaterialItems();
			}
		}
	}
	public class editListener implements ActionListener {

		private String edit;

		public editListener(String edit) {
			this.edit = edit;
		}
		@Override
		public void actionPerformed(ActionEvent e) {	
		
			if (edit.equals("machine")) {
				MachineItem masina = new MachineItem(0, view.getMachine(), view.getTimeMachine(), view.getCostMachine(),
						activity);
				model.SaveMachineItem(masina);
				model.getAllMachineItems();
			} else if(edit.equals("machineEdit")) {
				MachineItem act = view.getSelectedMachine();
				view.setCBMachine(act.getMachine().getMachine_id());
				view.setCostMachine(act.getLabor_cost_machine());
				view.setTimeMachine(act.getWork_time_machine());
				view.setAddListener(new editListener("MacEdit"));
				machine = act;
			} else if(edit.equals("MacEdit")) {
				machine.setMachine(view.getMachine());
				machine.setLabor_cost_machine(view.getCostMachine());
				machine.setWork_time_machine(view.getTimeMachine());
				model.EditMachineItem(machine);
				machine = null;
				view.setAddListener(new editListener("machine"));
				model.getAllMachine();
			} else if (edit.equals("material")) {
				MaterialItem material = new MaterialItem(0, view.getMaterial(), view.getQuantityMaterial(), view.getCostMaterial(),
						activity);
				model.SaveMaterialItem(material);
				model.getAllMaterialItems();
			} else if(edit.equals("materialEdit")) {
				MaterialItem act = view.getSelectedMaterial();
				view.setCBMaterial(act.getMaterial().getMaterial_id());
				view.setCostMaterial(act.getCost_material());
				view.setQuantityMaterial(act.getQuantity_material());
				view.setAddListener2(new editListener("MatEdit"));
				material = act;
			} else if(edit.equals("MatEdit")) {
				material.setMaterial(view.getMaterial());
				material.setCost_material(view.getCostMaterial());
				material.setQuantity_material(view.getQuantityMaterial());
				model.EditMaterialItem(material);
				material=null;
				view.setAddListener2(new editListener("material"));
				model.getAllMaterialItems();
			} else if (edit.equals("worker")) {
				WorkerItem work = new WorkerItem(0, view.getWorker(), view.getTimeWorker(),view.getCostWorker(), 
						activity);
				model.SaveWorkerItem(work);
				model.getAllWorkerItems();
			} else if(edit.equals("workerEdit")) {
				WorkerItem act = view.getSelectedWorker();
				view.setCBWorker(act.getWorker().getWorker_id());
				view.setCostWorker(act.getCost());
				view.setTimeWorker(act.getWork_time_worker());
				view.setAddListener3(new editListener("WorEdit"));
				worker=act;
			} else if(edit.equals("WorEdit")) {
				worker.setWorker(view.getWorker());
				worker.setCost(view.getCostWorker());
				worker.setWork_time_worker(view.getTimeWorker());
				model.EditWorkerItem(worker);
				worker=null;
				view.setAddListener3(new editListener("worker"));
				model.getAllWorkerItems(); 
			}
		}	
	}
}

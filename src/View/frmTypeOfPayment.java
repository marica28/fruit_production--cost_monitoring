package View;

import java.awt.HeadlessException;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class frmTypeOfPayment extends JFrame {
	private JSplitPane splitPane;
	private JScrollPane scrollPane;
	private JTable tblPayment;
	private JPanel panel;
	private JLabel lblPayment;
	private JTextField txtType;
	private JButton btnSave;

	public frmTypeOfPayment(String arg0) {
	
		super(arg0);
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmTypeOfPayment.class.getResource("/resources/iconfinder_business-work_5_2377641.png")));
		initialize();
	}

	private void initialize() {
		
		setBounds(50,50,390,263);
		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.1);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		tblPayment = new JTable();
		tblPayment.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblPayment.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblPayment);
		
		panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 121, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblPayment = new JLabel("Type of Payment:");
		lblPayment.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblPayment = new GridBagConstraints();
		gbc_lblPayment.insets = new Insets(0, 0, 0, 5);
		gbc_lblPayment.anchor = GridBagConstraints.EAST;
		gbc_lblPayment.gridx = 1;
		gbc_lblPayment.gridy = 1;
		panel.add(lblPayment, gbc_lblPayment);
		
		txtType = new JTextField();
		txtType.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtType = new GridBagConstraints();
		gbc_txtType.insets = new Insets(0, 0, 0, 5);
		gbc_txtType.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtType.gridx = 2;
		gbc_txtType.gridy = 1;
		panel.add(txtType, gbc_txtType);
		txtType.setColumns(10);
		
		btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon(frmTypeOfPayment.class.getResource("/resources/icons8-checked-24.png")));
		btnSave.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.gridx = 3;
		gbc_btnSave.gridy = 1;
		panel.add(btnSave, gbc_btnSave);
	}

	public String getTypeP() 
	{
		return txtType.getText();
	}
	public void setTypeP(String type)
	{
		txtType.setText(type);
	}
	public void setSaveListener(ActionListener save) 
	{
		btnSave.addActionListener(save);
	}

	public void setTableData(String[] header, Object[][] data) {
		tblPayment.setModel(new DefaultTableModel(data,header));
	}
	public void Clear() {
		txtType.setText("");
	}
	
}

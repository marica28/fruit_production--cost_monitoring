package View;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class frmMaterial extends JFrame {
	private JTable tblMaterial;
	private JTextField txtNameMat;
	private JTextField txtUnitM;
	private JButton btnSave;


	public frmMaterial(String arg0) throws HeadlessException {
		super(arg0);
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmMaterial.class.getResource("/resources/icons8-coins-100.png")));
		initialize();
	}

	private void initialize() {
		
		setBounds(50,50,429,489);
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		tblMaterial = new JTable();
		tblMaterial.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblMaterial.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblMaterial);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 152, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 1;
		panel.add(lblName, gbc_lblName);
		
		txtNameMat = new JTextField();
		txtNameMat.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtNameMat = new GridBagConstraints();
		gbc_txtNameMat.insets = new Insets(0, 0, 5, 5);
		gbc_txtNameMat.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNameMat.gridx = 2;
		gbc_txtNameMat.gridy = 1;
		panel.add(txtNameMat, gbc_txtNameMat);
		txtNameMat.setColumns(10);
		
		JLabel lblJedinicaMere = new JLabel("Unit measure:");
		lblJedinicaMere.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblJedinicaMere = new GridBagConstraints();
		gbc_lblJedinicaMere.insets = new Insets(0, 0, 0, 5);
		gbc_lblJedinicaMere.gridx = 1;
		gbc_lblJedinicaMere.gridy = 2;
		panel.add(lblJedinicaMere, gbc_lblJedinicaMere);
		
		txtUnitM = new JTextField();
		txtUnitM.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtUnitM = new GridBagConstraints();
		gbc_txtUnitM.insets = new Insets(0, 0, 0, 5);
		gbc_txtUnitM.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUnitM.gridx = 2;
		gbc_txtUnitM.gridy = 2;
		panel.add(txtUnitM, gbc_txtUnitM);
		txtUnitM.setColumns(10);
		
		btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon(frmMaterial.class.getResource("/resources/icons8-checked-24.png")));
		btnSave.setForeground(SystemColor.desktop);
		btnSave.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.gridx = 3;
		gbc_btnSave.gridy = 2;
		panel.add(btnSave, gbc_btnSave);
	}
	
	public String getName() {
		return txtNameMat.getText();
	}
	public void setName(String name) {
		txtNameMat.setText(name);
	}
	public String getUnitM() {
		return txtUnitM.getText();
	}
	public void setUnitM(String um) {
		txtUnitM.setText(um);
	}
	public void setSaveListener(ActionListener save) {
		btnSave.addActionListener(save);
	}
	public void setTableData(String[]header, Object[][]data) {
		tblMaterial.setModel(new DefaultTableModel(data,header));
	}
	public void ClearF() {
		txtUnitM.setText("");
		txtNameMat.setText("");
	}
	

}

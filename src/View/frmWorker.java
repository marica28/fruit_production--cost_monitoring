package View;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Controllers.TypeOfPaymentCtrl;
import Data.TypeOfPayment;
import Model.Model;

import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.DefaultComboBoxModel;

public class frmWorker extends JFrame {
	private JTextField txtNameWorker;
	private JTextField txtPhoneNumber;
	private JTable tblWorkers;
	private JButton btnSave;
	private JButton btnAddPayment;
	private JComboBox cbPayment;

	public frmWorker(String arg0) {
		
		super(arg0);
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmWorker.class.getResource("/resources/icons8-workers-100.png")));
		initialize();
	}

	private void initialize() {
		
		setBounds(50,50,639,403);
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 230, 182, 0, 136, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 0;
		panel.add(lblName, gbc_lblName);
		
		txtNameWorker = new JTextField();
		txtNameWorker.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtNameWorker = new GridBagConstraints();
		gbc_txtNameWorker.insets = new Insets(0, 0, 5, 5);
		gbc_txtNameWorker.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNameWorker.gridx = 2;
		gbc_txtNameWorker.gridy = 0;
		panel.add(txtNameWorker, gbc_txtNameWorker);
		txtNameWorker.setColumns(10);
		
		JLabel lblPhoneNumber = new JLabel("Phone number:");
		lblPhoneNumber.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblPhoneNumber = new GridBagConstraints();
		gbc_lblPhoneNumber.anchor = GridBagConstraints.EAST;
		gbc_lblPhoneNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblPhoneNumber.gridx = 1;
		gbc_lblPhoneNumber.gridy = 1;
		panel.add(lblPhoneNumber, gbc_lblPhoneNumber);
		
		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtPhoneNumber = new GridBagConstraints();
		gbc_txtPhoneNumber.insets = new Insets(0, 0, 5, 5);
		gbc_txtPhoneNumber.anchor = GridBagConstraints.NORTH;
		gbc_txtPhoneNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPhoneNumber.gridx = 2;
		gbc_txtPhoneNumber.gridy = 1;
		panel.add(txtPhoneNumber, gbc_txtPhoneNumber);
		txtPhoneNumber.setColumns(10);
		
		btnAddPayment = new JButton("Add Type of Payment");
		btnAddPayment.setIcon(new ImageIcon(frmWorker.class.getResource("/resources/icons8-plus-24.png")));
		btnAddPayment.setForeground(SystemColor.desktop);
		btnAddPayment.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		btnAddPayment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmTypeOfPayment view = new frmTypeOfPayment("Dodavanje tipa placanja");
				Model model = new Model();
				TypeOfPaymentCtrl tip = new TypeOfPaymentCtrl(view, model);
				view.setVisible(true);
			}
		});
		GridBagConstraints gbc_btnAddPayment = new GridBagConstraints();
		gbc_btnAddPayment.anchor = GridBagConstraints.WEST;
		gbc_btnAddPayment.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddPayment.gridx = 3;
		gbc_btnAddPayment.gridy = 1;
		panel.add(btnAddPayment, gbc_btnAddPayment);
		
		JLabel lblPayment = new JLabel("Type of Payment:");
		lblPayment.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblPayment = new GridBagConstraints();
		gbc_lblPayment.anchor = GridBagConstraints.EAST;
		gbc_lblPayment.insets = new Insets(0, 0, 0, 5);
		gbc_lblPayment.gridx = 1;
		gbc_lblPayment.gridy = 2;
		panel.add(lblPayment, gbc_lblPayment);
		
		cbPayment = new JComboBox();
		cbPayment.setModel(new DefaultComboBoxModel(new String[] {"Select"}));
		cbPayment.setFont(new Font("Dialog", Font.PLAIN, 16));
		cbPayment.setBackground(new Color(255, 222, 173));
		GridBagConstraints gbc_cbPayment = new GridBagConstraints();
		gbc_cbPayment.insets = new Insets(0, 0, 0, 5);
		gbc_cbPayment.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbPayment.gridx = 2;
		gbc_cbPayment.gridy = 2;
		panel.add(cbPayment, gbc_cbPayment);
		
		btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon(frmWorker.class.getResource("/resources/icons8-checked-24.png")));
		btnSave.setForeground(SystemColor.desktop);
		btnSave.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSave.insets = new Insets(0, 0, 0, 5);
		gbc_btnSave.gridx = 3;
		gbc_btnSave.gridy = 2;
		panel.add(btnSave, gbc_btnSave);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		tblWorkers = new JTable();
		tblWorkers.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblWorkers.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblWorkers);
	}
	public String getNameWorker()
	{
		return txtNameWorker.getText();
	}
	public void setNameWorker(String name)
	{
		txtNameWorker.setText(name);
	}
	public String getPhoneNumber()
	{
		return txtPhoneNumber.getText();
	}
	public void setPhoneNumber(String number)
	{
		txtPhoneNumber.setText(number);
	}
	public void setSaveListener(ActionListener save)
	{
		btnSave.addActionListener(save);
	}
	public void setTableData(String[] header, Object[][] data)
	{
		tblWorkers.setModel(new DefaultTableModel(data,header));
	}
	public Object getTypePayment()
	{
		return cbPayment.getSelectedItem();
	}
	public void setTypePaymentCB(ArrayList<TypeOfPayment>type)
	{
		for (TypeOfPayment t:type)
		{
			cbPayment.addItem(t);
		}
	}
	public void ClearF() {
		txtPhoneNumber.setText("");
		txtNameWorker.setText("");
	}
}

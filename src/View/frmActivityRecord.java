
package View;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import Data.ActivityRecord;
import Data.Machine;
import Data.MachineItem;
import Data.Material;
import Data.MaterialItem;
import Data.Worker;
import Data.WorkerItem;
import Data.TypeOfPayment;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JComboBox;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.JTabbedPane;
import java.awt.event.ActionEvent;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;

public class frmActivityRecord extends JFrame {
	private JTextField txtDate;
	private JTextField txtName;
	private JTextField txtDecsription;
	private JTextField txtLocation;
	private JTextField txtTimeMachine;
	private JTextField txtCostMachine;
	private JTextField txtQuantityMaterial;
	private JTextField txtCostMaterial;
	private JTextField txtTimeWorker;
	private JTextField txtCostWorker;
	private JButton btnAddWorker;
	private JButton btnSave;
	private JButton btnAddMachine;
	private JButton btnAddMaterial;
	private JComboBox cbWorkers;
	private JComboBox cbMaterials;
	private JComboBox cbMachines;
	private JLabel label_4;
	private JButton btnFinish;
	private JSplitPane splitPane_2;
	private JPanel panel_2;
	private JTabbedPane tabbedPane;
	private JScrollPane scrollPane;
	private JTable tblMachine;
	private JScrollPane scrollPane_1;
	private JTable tblMaterial;
	private JScrollPane scrollPane_2;
	private JTable tblWorker;
	private JPopupMenu popupMenu;
	private JMenuItem mntmEdit;
	private JPopupMenu popupMenu_1;
	private JMenuItem mntmEdit_1;
	private JPopupMenu popupMenu_2;
	private JMenuItem mntmEdit_2;
	private JLabel lblUkupniTroskoviAktivnosti;
	private JLabel lblSum;

	public frmActivityRecord(String title) throws HeadlessException {
		super(title);
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmActivityRecord.class.getResource("/resources/iconfinder_business-work_9_2377637.png")));
		initialize();
	}

	private void initialize() {
		
		setBounds(50,50,773,654);
		getContentPane().setLayout(new BorderLayout(0, 0));
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.1);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 221, 143, 21, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblDateOfActivity = new JLabel("Date of Activity:");
		lblDateOfActivity.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblDateOfActivity = new GridBagConstraints();
		gbc_lblDateOfActivity.insets = new Insets(0, 0, 5, 5);
		gbc_lblDateOfActivity.gridx = 1;
		gbc_lblDateOfActivity.gridy = 1;
		panel.add(lblDateOfActivity, gbc_lblDateOfActivity);
		
		txtDate = new JTextField();
		txtDate.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtDate.setColumns(10);
		GridBagConstraints gbc_txtDate = new GridBagConstraints();
		gbc_txtDate.insets = new Insets(0, 0, 5, 5);
		gbc_txtDate.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDate.gridx = 3;
		gbc_txtDate.gridy = 1;
		panel.add(txtDate, gbc_txtDate);
		
		JLabel lblNameOfActivity = new JLabel("Name of Activity:");
		lblNameOfActivity.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNameOfActivity = new GridBagConstraints();
		gbc_lblNameOfActivity.insets = new Insets(0, 0, 5, 5);
		gbc_lblNameOfActivity.gridx = 1;
		gbc_lblNameOfActivity.gridy = 2;
		panel.add(lblNameOfActivity, gbc_lblNameOfActivity);
		
		txtName = new JTextField();
		txtName.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtName.setColumns(10);
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.insets = new Insets(0, 0, 5, 5);
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.gridx = 3;
		gbc_txtName.gridy = 2;
		panel.add(txtName, gbc_txtName);
		
		JLabel lblDescriptionOfActivity = new JLabel("Description of Activity:");
		lblDescriptionOfActivity.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblDescriptionOfActivity = new GridBagConstraints();
		gbc_lblDescriptionOfActivity.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescriptionOfActivity.gridx = 1;
		gbc_lblDescriptionOfActivity.gridy = 3;
		panel.add(lblDescriptionOfActivity, gbc_lblDescriptionOfActivity);
		
		txtDecsription = new JTextField();
		txtDecsription.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtDecsription.setColumns(10);
		GridBagConstraints gbc_txtDecsription = new GridBagConstraints();
		gbc_txtDecsription.insets = new Insets(0, 0, 5, 5);
		gbc_txtDecsription.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDecsription.gridx = 3;
		gbc_txtDecsription.gridy = 3;
		panel.add(txtDecsription, gbc_txtDecsription);
		
		JLabel lblLocationOfActivity = new JLabel("Location of Activity:");
		lblLocationOfActivity.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblLocationOfActivity = new GridBagConstraints();
		gbc_lblLocationOfActivity.insets = new Insets(0, 0, 0, 5);
		gbc_lblLocationOfActivity.gridx = 1;
		gbc_lblLocationOfActivity.gridy = 4;
		panel.add(lblLocationOfActivity, gbc_lblLocationOfActivity);
		
		txtLocation = new JTextField();
		txtLocation.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtLocation = new GridBagConstraints();
		gbc_txtLocation.insets = new Insets(0, 0, 0, 5);
		gbc_txtLocation.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtLocation.gridx = 3;
		gbc_txtLocation.gridy = 4;
		panel.add(txtLocation, gbc_txtLocation);
		txtLocation.setColumns(10);
		
		btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon(frmActivityRecord.class.getResource("/resources/icons8-checked-24.png")));
		btnSave.setForeground(SystemColor.desktop);
		btnSave.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSave.insets = new Insets(0, 0, 0, 5);
		gbc_btnSave.gridx = 4;
		gbc_btnSave.gridy = 4;
		panel.add(btnSave, gbc_btnSave);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.1);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setRightComponent(splitPane_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(224, 255, 255));
		splitPane_1.setLeftComponent(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{147, 73, 59, 153, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblWorkTime = new JLabel("Work time:");
		lblWorkTime.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblWorkTime = new GridBagConstraints();
		gbc_lblWorkTime.insets = new Insets(0, 0, 5, 5);
		gbc_lblWorkTime.gridx = 1;
		gbc_lblWorkTime.gridy = 0;
		panel_1.add(lblWorkTime, gbc_lblWorkTime);
		
		JLabel lblCost = new JLabel("Cost:");
		lblCost.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblCost = new GridBagConstraints();
		gbc_lblCost.insets = new Insets(0, 0, 5, 5);
		gbc_lblCost.gridx = 2;
		gbc_lblCost.gridy = 0;
		panel_1.add(lblCost, gbc_lblCost);
		
		cbMachines = new JComboBox();
		cbMachines.setModel(new DefaultComboBoxModel(new String[] {"Select Machine"}));
		cbMachines.setFont(new Font("Dialog", Font.PLAIN, 17));
		cbMachines.setBackground(new Color(255, 222, 173));
		GridBagConstraints gbc_cbMachines = new GridBagConstraints();
		gbc_cbMachines.insets = new Insets(0, 0, 5, 5);
		gbc_cbMachines.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbMachines.gridx = 0;
		gbc_cbMachines.gridy = 1;
		panel_1.add(cbMachines, gbc_cbMachines);
		
		txtTimeMachine = new JTextField();
		txtTimeMachine.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtTimeMachine.setColumns(10);
		GridBagConstraints gbc_txtTimeMachine = new GridBagConstraints();
		gbc_txtTimeMachine.insets = new Insets(0, 0, 5, 5);
		gbc_txtTimeMachine.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTimeMachine.gridx = 1;
		gbc_txtTimeMachine.gridy = 1;
		panel_1.add(txtTimeMachine, gbc_txtTimeMachine);
		
		txtCostMachine = new JTextField();
		txtCostMachine.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtCostMachine.setColumns(10);
		GridBagConstraints gbc_txtCostMachine = new GridBagConstraints();
		gbc_txtCostMachine.insets = new Insets(0, 0, 5, 5);
		gbc_txtCostMachine.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCostMachine.gridx = 2;
		gbc_txtCostMachine.gridy = 1;
		panel_1.add(txtCostMachine, gbc_txtCostMachine);
		
		btnAddMachine = new JButton("Add Machine");
		btnAddMachine.setIcon(new ImageIcon(frmActivityRecord.class.getResource("/resources/icons8-plus-24.png")));
		btnAddMachine.setForeground(SystemColor.desktop);
		btnAddMachine.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnAddMachine = new GridBagConstraints();
		gbc_btnAddMachine.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAddMachine.insets = new Insets(0, 0, 5, 0);
		gbc_btnAddMachine.gridx = 3;
		gbc_btnAddMachine.gridy = 1;
		panel_1.add(btnAddMachine, gbc_btnAddMachine);
		
		label_4 = new JLabel("");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 0;
		gbc_label_4.gridy = 2;
		panel_1.add(label_4, gbc_label_4);
		
		JLabel lblQuantity = new JLabel("Quantity:");
		lblQuantity.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblQuantity = new GridBagConstraints();
		gbc_lblQuantity.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantity.gridx = 1;
		gbc_lblQuantity.gridy = 2;
		panel_1.add(lblQuantity, gbc_lblQuantity);
		
		JLabel lblCost_1 = new JLabel("Cost:");
		lblCost_1.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblCost_1 = new GridBagConstraints();
		gbc_lblCost_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblCost_1.gridx = 2;
		gbc_lblCost_1.gridy = 2;
		panel_1.add(lblCost_1, gbc_lblCost_1);
		
		cbMaterials = new JComboBox();
		cbMaterials.setModel(new DefaultComboBoxModel(new String[] {"Select Material"}));
		cbMaterials.setFont(new Font("Dialog", Font.PLAIN, 17));
		cbMaterials.setBackground(new Color(255, 222, 173));
		GridBagConstraints gbc_cbMaterials = new GridBagConstraints();
		gbc_cbMaterials.insets = new Insets(0, 0, 5, 5);
		gbc_cbMaterials.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbMaterials.gridx = 0;
		gbc_cbMaterials.gridy = 3;
		panel_1.add(cbMaterials, gbc_cbMaterials);
		
		txtQuantityMaterial = new JTextField();
		txtQuantityMaterial.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtQuantityMaterial.setColumns(10);
		GridBagConstraints gbc_txtQuantityMaterial = new GridBagConstraints();
		gbc_txtQuantityMaterial.insets = new Insets(0, 0, 5, 5);
		gbc_txtQuantityMaterial.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtQuantityMaterial.gridx = 1;
		gbc_txtQuantityMaterial.gridy = 3;
		panel_1.add(txtQuantityMaterial, gbc_txtQuantityMaterial);
		
		txtCostMaterial = new JTextField();
		txtCostMaterial.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtCostMaterial.setColumns(10);
		GridBagConstraints gbc_txtCostMaterial = new GridBagConstraints();
		gbc_txtCostMaterial.insets = new Insets(0, 0, 5, 5);
		gbc_txtCostMaterial.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCostMaterial.gridx = 2;
		gbc_txtCostMaterial.gridy = 3;
		panel_1.add(txtCostMaterial, gbc_txtCostMaterial);
		
		btnAddMaterial = new JButton("Add Material");
		btnAddMaterial.setIcon(new ImageIcon(frmActivityRecord.class.getResource("/resources/icons8-plus-24.png")));
		btnAddMaterial.setForeground(SystemColor.desktop);
		btnAddMaterial.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnAddMaterial = new GridBagConstraints();
		gbc_btnAddMaterial.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAddMaterial.insets = new Insets(0, 0, 5, 0);
		gbc_btnAddMaterial.gridx = 3;
		gbc_btnAddMaterial.gridy = 3;
		panel_1.add(btnAddMaterial, gbc_btnAddMaterial);
		
		JLabel lblWorkTime_1 = new JLabel("Work time:");
		lblWorkTime_1.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblWorkTime_1 = new GridBagConstraints();
		gbc_lblWorkTime_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblWorkTime_1.gridx = 1;
		gbc_lblWorkTime_1.gridy = 4;
		panel_1.add(lblWorkTime_1, gbc_lblWorkTime_1);
		
		JLabel lblCenaRada = new JLabel("Cost:");
		lblCenaRada.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblCenaRada = new GridBagConstraints();
		gbc_lblCenaRada.insets = new Insets(0, 0, 5, 5);
		gbc_lblCenaRada.gridx = 2;
		gbc_lblCenaRada.gridy = 4;
		panel_1.add(lblCenaRada, gbc_lblCenaRada);
		
		cbWorkers = new JComboBox();
		cbWorkers.setModel(new DefaultComboBoxModel(new String[] {"Select Worker"}));
		cbWorkers.setFont(new Font("Dialog", Font.PLAIN, 17));
		cbWorkers.setBackground(new Color(255, 222, 173));
		GridBagConstraints gbc_cbWorkers = new GridBagConstraints();
		gbc_cbWorkers.insets = new Insets(0, 0, 5, 5);
		gbc_cbWorkers.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbWorkers.gridx = 0;
		gbc_cbWorkers.gridy = 5;
		panel_1.add(cbWorkers, gbc_cbWorkers);
		
		txtTimeWorker = new JTextField();
		txtTimeWorker.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtTimeWorker.setColumns(10);
		GridBagConstraints gbc_txtTimeWorker = new GridBagConstraints();
		gbc_txtTimeWorker.insets = new Insets(0, 0, 5, 5);
		gbc_txtTimeWorker.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTimeWorker.gridx = 1;
		gbc_txtTimeWorker.gridy = 5;
		panel_1.add(txtTimeWorker, gbc_txtTimeWorker);
		
		txtCostWorker = new JTextField();
		txtCostWorker.setFont(new Font("Dialog", Font.PLAIN, 16));
		txtCostWorker.setColumns(10);
		GridBagConstraints gbc_txtCostWorker = new GridBagConstraints();
		gbc_txtCostWorker.insets = new Insets(0, 0, 5, 5);
		gbc_txtCostWorker.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCostWorker.gridx = 2;
		gbc_txtCostWorker.gridy = 5;
		panel_1.add(txtCostWorker, gbc_txtCostWorker);
		
		btnAddWorker = new JButton("Add Worker");
		btnAddWorker.setIcon(new ImageIcon(frmActivityRecord.class.getResource("/resources/icons8-plus-24.png")));
		btnAddWorker.setForeground(SystemColor.desktop);
		btnAddWorker.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnAddWorker = new GridBagConstraints();
		gbc_btnAddWorker.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAddWorker.insets = new Insets(0, 0, 5, 0);
		gbc_btnAddWorker.gridx = 3;
		gbc_btnAddWorker.gridy = 5;
		panel_1.add(btnAddWorker, gbc_btnAddWorker);
		
		btnFinish = new JButton("Finish");
		btnFinish.setIcon(new ImageIcon(frmActivityRecord.class.getResource("/resources/icons8-delete-24.png")));
		btnFinish.setForeground(SystemColor.desktop);
		btnFinish.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnFinish = new GridBagConstraints();
		gbc_btnFinish.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnFinish.gridx = 3;
		gbc_btnFinish.gridy = 6;
		panel_1.add(btnFinish, gbc_btnFinish);
		
		splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.8);
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setRightComponent(splitPane_2);
		
		panel_2 = new JPanel();
		panel_2.setBackground(new Color(224, 255, 255));
		splitPane_2.setRightComponent(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{193, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		lblUkupniTroskoviAktivnosti = new JLabel("Total Cost:");
		lblUkupniTroskoviAktivnosti.setFont(new Font("Dialog", Font.BOLD, 17));
		GridBagConstraints gbc_lblUkupniTroskoviAktivnosti = new GridBagConstraints();
		gbc_lblUkupniTroskoviAktivnosti.insets = new Insets(0, 0, 0, 5);
		gbc_lblUkupniTroskoviAktivnosti.anchor = GridBagConstraints.WEST;
		gbc_lblUkupniTroskoviAktivnosti.gridx = 0;
		gbc_lblUkupniTroskoviAktivnosti.gridy = 0;
		panel_2.add(lblUkupniTroskoviAktivnosti, gbc_lblUkupniTroskoviAktivnosti);
		
		lblSum = new JLabel("SUM");
		lblSum.setFont(new Font("Dialog", Font.BOLD, 17));
		GridBagConstraints gbc_lblSum = new GridBagConstraints();
		gbc_lblSum.anchor = GridBagConstraints.EAST;
		gbc_lblSum.gridx = 9;
		gbc_lblSum.gridy = 0;
		panel_2.add(lblSum, gbc_lblSum);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(new Color(255, 222, 173));
		tabbedPane.setFont(new Font("Dialog", Font.BOLD, 17));
		splitPane_2.setLeftComponent(tabbedPane);
		
		scrollPane = new JScrollPane();
		tabbedPane.addTab("Machines", null, scrollPane, null);
		
		tblMachine = new JTable();
		tblMachine.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblMachine.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblMachine);
		
		popupMenu = new JPopupMenu();
		addPopup(tblMachine, popupMenu);
		
		mntmEdit = new JMenuItem("Izmeni");
		popupMenu.add(mntmEdit);
		
		scrollPane_1 = new JScrollPane();
		tabbedPane.addTab("Materials", null, scrollPane_1, null);
		
		tblMaterial = new JTable();
		tblMaterial.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblMaterial.setFillsViewportHeight(true);
		scrollPane_1.setViewportView(tblMaterial);
		
		popupMenu_1 = new JPopupMenu();
		addPopup(tblMaterial, popupMenu_1);
		
		mntmEdit_1 = new JMenuItem("Izmeni");
		popupMenu_1.add(mntmEdit_1);
		
		scrollPane_2 = new JScrollPane();
		tabbedPane.addTab("Workers", null, scrollPane_2, null);
		
		tblWorker = new JTable();
		tblWorker.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblWorker.setFillsViewportHeight(true);
		scrollPane_2.setViewportView(tblWorker);
		
		popupMenu_2 = new JPopupMenu();
		addPopup(tblWorker, popupMenu_2);
		
		mntmEdit_2 = new JMenuItem("Izmeni");
		popupMenu_2.add(mntmEdit_2);
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////SET AND GET	
	public long getDateAct() {
		long date1 =0;
		try {
			Date date = new SimpleDateFormat("dd.MM.yyyy").parse(txtDate.getText());
			date1 = date.getTime()/1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date1;
	}
	public void setDateAct(String string) {
			txtDate.setText(string);
	}
	public String getNameAct() {
		return txtName.getText();
	}
	public void setNameAct(String name) {
		txtName.setText(name);
	}
	public String getDescriptionAct() {
		return txtDecsription.getText();
	}
	public void setDescriptionAct(String des) {
		txtDecsription.setText(des);
	}
	public String getLocationAct() {
		return txtLocation.getText();
	}
	public void setLocationAct(String loc) {
		txtLocation.setText(loc);
	}
	public Float getTimeMachine() {
		return Float.parseFloat(txtTimeMachine.getText());
	}
	public void setTimeMachine(Float time) {
		txtTimeMachine.setText(Float.toString(time));
	}
	public Float getCostMachine() {
		return Float.parseFloat(txtCostMachine.getText());
	}
	public void setCostMachine(Float cost) {
		txtCostMachine.setText(Float.toString(cost));
	}
	public Float getQuantityMaterial() {
		return Float.parseFloat(txtQuantityMaterial.getText());
	}
	public void setQuantityMaterial(Float quan) {
		txtQuantityMaterial.setText(Float.toString(quan));
	}
	public Float getCostMaterial() {
		return Float.parseFloat(txtCostMaterial.getText());
	}
	public void setCostMaterial(Float cost) {
		txtCostMaterial.setText(Float.toString(cost));
	}
	public Float getTimeWorker() {
		return Float.parseFloat(txtTimeWorker.getText());
	}
	public void setTimeWorker(Float time) {
		txtTimeWorker.setText(Float.toString(time));
	}
	public Float getCostWorker() {
		return Float.parseFloat(txtCostWorker.getText());
	}
	public void setCostWorker(Float cost) {
		txtCostWorker.setText(Float.toString(cost));
	}
	public void setSUMActivity(Double sum) {
		lblSum.setText(Double.toString(sum));
	}
///////////////////////////////////////////////////////////////////////////////////CB	
	public void setCBMachine(ArrayList<Machine>machines) {
		for(Machine mas:machines) {
			cbMachines.addItem(mas);
		}
	}
	public void setCBMaterial(ArrayList<Material>materials) {
		for(Material mat:materials) {
			cbMaterials.addItem(mat);
		}
	}
	public void setCBWorker(ArrayList<Worker>workers) {
		for(Worker rad:workers) {
			cbWorkers.addItem(rad);
		}
	}
	public void setCBMachine(int id) {
		for(int i=0;i<cbMachines.getItemCount();i++) {
			if(((Machine)cbMachines.getItemAt(i)).getMachine_id()==id) 
			{
				cbMachines.setSelectedIndex(i);
			}
		}
	}
	public void setCBMaterial(int id) {
		for(int i=0;i<cbMaterials.getItemCount();i++) {
			if(((Material)cbMaterials.getItemAt(i)).getMaterial_id()==id) 
			{
				cbMaterials.setSelectedIndex(i);
			}
		}
	}
	public void setCBWorker(int id) {
		for(int i=0;i<cbWorkers.getItemCount();i++) {
			if(((Worker)cbWorkers.getItemAt(i)).getWorker_id()==id) 
			{
				cbWorkers.setSelectedIndex(i);
			}
		}
	}
	public Machine getMachine() {
		return (Machine) cbMachines.getSelectedItem();
	}
	public Material getMaterial() {
		return (Material) cbMaterials.getSelectedItem();
	}
	public Worker getWorker() {
		return (Worker) cbWorkers.getSelectedItem();
	}

///////////////////////////////////////////////////////////////////////////TBL
	public void setTableMachine(String[] header, Object[][] data)
	{
		tblMachine.setModel(new DefaultTableModel(data,header));
	}
	public void setTableMaterial(String[] header2, Object[][] data2)
	{
		tblMaterial.setModel(new DefaultTableModel(data2,header2));
	}
	public void setTableWorker(String[] header3, Object[][] data3)
	{
		tblWorker.setModel(new DefaultTableModel(data3,header3));
	}
	public MachineItem getSelectedMachine() {
		return (MachineItem) tblMachine.getValueAt(tblMachine.getSelectedRow(), 1);
	}
	public MaterialItem getSelectedMaterial() {
		return (MaterialItem) tblMaterial.getValueAt(tblMaterial.getSelectedRow(), 1);
	}
	public WorkerItem getSelectedWorker() {
		return (WorkerItem) tblWorker.getValueAt(tblWorker.getSelectedRow(), 1);
	}
//////////////////////////////////////////////////////////////////////////BTN
	public void setSaveListener(ActionListener save) {
		btnSave.addActionListener(save);
	}
	public void setAddListener(ActionListener add) {
		btnRemoveListeners();
		btnAddMachine.addActionListener(add);
	}
	private void btnRemoveListeners() {
		for(ActionListener al: btnAddMachine.getActionListeners()) {
			btnAddMachine.removeActionListener(al);
		}
	}
	public void setAddListener2(ActionListener add) {
		btnRemoveListenersMat();
		btnAddMaterial.addActionListener(add);
	}
	private void btnRemoveListenersMat() {
		for(ActionListener al: btnAddMaterial.getActionListeners()) {
			btnAddMaterial.removeActionListener(al);
		}
	}
	public void setAddListener3(ActionListener add) {
		btnRemoveListenersRad();
		btnAddWorker.addActionListener(add);
	}
	private void btnRemoveListenersRad() {
		for(ActionListener al: btnAddWorker.getActionListeners()) {
			btnAddWorker.removeActionListener(al);
		}
	}
	public void setFinishListener(ActionListener finish) {
		btnFinish.addActionListener(finish);
	}
	public void setEditActivity(String s) {
		btnSave.setText(s);
	}
	public void setFinishDisable() {
		btnFinish.setEnabled(false);
	}
//////////////////////////////////////////////////////////////////////////////CLEAR
	public void ClearFields() {
		txtDate.setText("");
		txtName.setText("");
		txtLocation.setText("");
		txtDecsription.setText("");
		txtCostMachine.setText("");
		txtCostMaterial.setText("");
		txtCostWorker.setText("");
		txtTimeMachine.setText("");
		txtTimeWorker.setText("");
		txtQuantityMaterial.setText("");
	}
	public void ClearTables() {
		DefaultTableModel model = (DefaultTableModel) tblMachine.getModel();
		model.setRowCount(0);
		DefaultTableModel model1 = (DefaultTableModel) tblMaterial.getModel();
		model1.setRowCount(0);
		DefaultTableModel model2 = (DefaultTableModel) tblWorker.getModel();
		model2.setRowCount(0);
	}
/////////////////////////////////////////////////////////////////////////////////////////////////POPUP MENU
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (component instanceof JTable) {
					int red=((JTable) component).rowAtPoint(e.getPoint());
					if(red>=0 && red<((JTable) component).getRowCount()) {
						((JTable) component).setRowSelectionInterval(red, red);
					} else {
						((JTable) component).clearSelection();
					}
				if(e.isPopupTrigger() && ((JTable) component).getSelectedRow()>=0) {
					showMenu(e);
					}
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	public void EditMachine(ActionListener edit) {
		mntmEdit.addActionListener(edit);
	}
	public void EditMaterial(ActionListener edit) {
		mntmEdit_1.addActionListener(edit);
	}
	public void EditWorker(ActionListener edit) {
		mntmEdit_2.addActionListener(edit);
	}
	
}

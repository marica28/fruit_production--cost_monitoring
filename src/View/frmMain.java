package View;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import Controllers.ActivityCtrl;
import Controllers.MachineCtrl;
import Controllers.MaterialCtrl;
import Controllers.ReportsCtrl;
import Controllers.WorkerCtrl;
import Data.ActivityRecord;
import Model.Model;

import java.awt.Insets;
import javax.swing.JComboBox;
import java.awt.FlowLayout;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Rectangle;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTabbedPane;
import javax.swing.JMenuItem;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Window.Type;

public class frmMain extends JFrame {
	private JSplitPane splitPane;
	private JPanel panel;
	private JButton btnAddMachine;
	private JButton btnAddMaterial;
	private JButton btnAddWorker;
	private JButton btnAddActivity;
	private JButton btnReports;
	private JSplitPane splitPane_1;
	private JScrollPane scrollPane;
	private JTabbedPane tabbedPane;
	private JPanel panel_Date;
	private JPanel panel_Location;
	private JPanel panel_Name;
	private JLabel lblFROM;
	private JTextField txtDateFrom;
	private JLabel lblTO;
	private JTextField txtDateTo;
	private JButton btnSearch;
	private JLabel lblNameAct;
	private JTextField txtNameAct;
	private JButton btnSearchName;
	private JLabel lblLocation;
	private JTextField txtLocation;
	private JButton btnSearchLocation;
	private JTable tblActivity;
	private JPopupMenu popupMenu;
	private JMenuItem mntmEdit;
	private JButton btnEndSearch;
	private JButton btnEndSearchName;
	private JButton btnEndSearchLocation;

	
	public frmMain(String arg0) throws HeadlessException {
		super(arg0);
		getContentPane().setFont(new Font("Dialog", Font.PLAIN, 18));
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmMain.class.getResource("/resources/icons8-raspberry-filled-96.png")));
		initialize();
	}
	
	public void initialize() {
		
		setBounds(0,0,1038,600);
		setFont(new Font("Dialog", Font.BOLD, 18));
		setForeground(SystemColor.menu);
		
		splitPane = new JSplitPane();
		splitPane.setToolTipText("");
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		btnReports = new JButton("Reports");
		btnReports.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-increase-32.png")));
		btnReports.setForeground(Color.DARK_GRAY);
		btnReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmReports view = new frmReports("Reports - All Costs");
				Model model = new Model();
				ReportsCtrl con = new ReportsCtrl(model, view);
				view.setVisible(true);
			}
		});
		
		btnAddActivity = new JButton("Add Activity");
		btnAddActivity.setSelectedIcon(null);
		btnAddActivity.setHorizontalAlignment(SwingConstants.LEFT);
		btnAddActivity.setVerticalAlignment(SwingConstants.BOTTOM);
		btnAddActivity.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-timeline-32.png")));
		btnAddActivity.setForeground(Color.DARK_GRAY);
		btnAddActivity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmActivityRecord view = new frmActivityRecord("Add New Activity");
				Model model = new Model();
				ActivityCtrl akti = new ActivityCtrl(view, model);
				view.setVisible(true);
			}
		});
		btnAddActivity.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		GridBagConstraints gbc_btnAddActivity = new GridBagConstraints();
		gbc_btnAddActivity.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddActivity.gridx = 0;
		gbc_btnAddActivity.gridy = 0;
		panel.add(btnAddActivity, gbc_btnAddActivity);
		
		btnAddWorker = new JButton("Add Worker");
		btnAddWorker.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-workers-32.png")));
		btnAddWorker.setForeground(Color.DARK_GRAY);
		btnAddWorker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frmWorker view = new frmWorker("Add New Worker");
				Model model = new Model();
				WorkerCtrl radnik = new WorkerCtrl(view, model);
				view.setVisible(true);
			}
		});
		
		btnAddMachine = new JButton("Add Machine");
		btnAddMachine.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-services-32.png")));
		btnAddMachine.setForeground(Color.DARK_GRAY);
		btnAddMachine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMachine view = new frmMachine("Add New Machine");
				Model model = new Model();
				MachineCtrl mas = new MachineCtrl(view, model);
				view.setVisible(true);
			}
		});
		btnAddMachine.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		GridBagConstraints gbc_btnAddMachine = new GridBagConstraints();
		gbc_btnAddMachine.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddMachine.gridx = 1;
		gbc_btnAddMachine.gridy = 0;
		panel.add(btnAddMachine, gbc_btnAddMachine);
		
		btnAddMaterial = new JButton("Add Material");
		btnAddMaterial.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-sales-performance-32.png")));
		btnAddMaterial.setForeground(Color.DARK_GRAY);
		btnAddMaterial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmMaterial view = new frmMaterial("Add New Material");
				Model model = new Model();
				MaterialCtrl mat = new MaterialCtrl(view, model);
				view.setVisible(true);
			}
		});
		btnAddMaterial.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		GridBagConstraints gbc_btnAddMaterial = new GridBagConstraints();
		gbc_btnAddMaterial.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddMaterial.gridx = 2;
		gbc_btnAddMaterial.gridy = 0;
		panel.add(btnAddMaterial, gbc_btnAddMaterial);
		btnAddWorker.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		GridBagConstraints gbc_btnAddWorker = new GridBagConstraints();
		gbc_btnAddWorker.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddWorker.gridx = 3;
		gbc_btnAddWorker.gridy = 0;
		panel.add(btnAddWorker, gbc_btnAddWorker);
		btnReports.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		GridBagConstraints gbc_btnReports = new GridBagConstraints();
		gbc_btnReports.gridx = 4;
		gbc_btnReports.gridy = 0;
		panel.add(btnReports, gbc_btnReports);
		
		splitPane_1 = new JSplitPane();
		splitPane.setRightComponent(splitPane_1);
		
		scrollPane = new JScrollPane();
		splitPane_1.setRightComponent(scrollPane);
		
		tblActivity = new JTable();
		tblActivity.setBackground(Color.WHITE);
		tblActivity.setBorder(null);
		tblActivity.setFont(new Font("Dialog", Font.PLAIN, 13));
		tblActivity.setToolTipText("");
		tblActivity.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblActivity);
		
		popupMenu = new JPopupMenu();
		addPopup(tblActivity, popupMenu);
		
		mntmEdit = new JMenuItem("Edit");
		mntmEdit.setBackground(new Color(255, 222, 173));
		mntmEdit.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-edit-24.png")));
		popupMenu.add(mntmEdit);
		
		tabbedPane = new JTabbedPane(JTabbedPane.BOTTOM);
		tabbedPane.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 18));
		splitPane_1.setLeftComponent(tabbedPane);
		
		panel_Date = new JPanel();
		panel_Date.setForeground(Color.DARK_GRAY);
		panel_Date.setBackground(new Color(224, 255, 255));
		tabbedPane.addTab("Search DATE", new ImageIcon(frmMain.class.getResource("/resources/icons8-calendar-32.png")), panel_Date, null);
		tabbedPane.setForegroundAt(0, Color.BLACK);
		tabbedPane.setBackgroundAt(0, new Color(255, 222, 173));
		GridBagLayout gbl_panel_Date = new GridBagLayout();
		gbl_panel_Date.columnWidths = new int[]{0, 0};
		gbl_panel_Date.rowHeights = new int[]{36, 0, 0, 0, 0, 38, 0, 24, 0, 0};
		gbl_panel_Date.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_Date.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_Date.setLayout(gbl_panel_Date);
		
		lblFROM = new JLabel("FROM:");
		lblFROM.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblFROM = new GridBagConstraints();
		gbc_lblFROM.insets = new Insets(0, 0, 5, 0);
		gbc_lblFROM.gridx = 0;
		gbc_lblFROM.gridy = 1;
		panel_Date.add(lblFROM, gbc_lblFROM);
		
		txtDateFrom = new JTextField();
		txtDateFrom.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtDateFrom = new GridBagConstraints();
		gbc_txtDateFrom.insets = new Insets(0, 0, 5, 0);
		gbc_txtDateFrom.gridx = 0;
		gbc_txtDateFrom.gridy = 2;
		panel_Date.add(txtDateFrom, gbc_txtDateFrom);
		txtDateFrom.setColumns(10);
		
		lblTO = new JLabel("TO:");
		lblTO.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblTO = new GridBagConstraints();
		gbc_lblTO.insets = new Insets(0, 0, 5, 0);
		gbc_lblTO.gridx = 0;
		gbc_lblTO.gridy = 3;
		panel_Date.add(lblTO, gbc_lblTO);
		
		txtDateTo = new JTextField();
		txtDateTo.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtDateTo = new GridBagConstraints();
		gbc_txtDateTo.insets = new Insets(0, 0, 5, 0);
		gbc_txtDateTo.anchor = GridBagConstraints.NORTH;
		gbc_txtDateTo.gridx = 0;
		gbc_txtDateTo.gridy = 4;
		panel_Date.add(txtDateTo, gbc_txtDateTo);
		txtDateTo.setColumns(10);
		
		btnSearch = new JButton("Search");
		btnSearch.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-search-24.png")));
		btnSearch.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSearch = new GridBagConstraints();
		gbc_btnSearch.insets = new Insets(0, 0, 5, 0);
		gbc_btnSearch.gridx = 0;
		gbc_btnSearch.gridy = 6;
		panel_Date.add(btnSearch, gbc_btnSearch);
		
		btnEndSearch = new JButton("End Search");
		btnEndSearch.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-delete-24.png")));
		btnEndSearch.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnEndSearch = new GridBagConstraints();
		gbc_btnEndSearch.gridx = 0;
		gbc_btnEndSearch.gridy = 8;
		panel_Date.add(btnEndSearch, gbc_btnEndSearch);
		
		panel_Name = new JPanel();
		panel_Name.setForeground(Color.DARK_GRAY);
		panel_Name.setBackground(new Color(224, 255, 255));
		tabbedPane.addTab("Search NAME", new ImageIcon(frmMain.class.getResource("/resources/icons8-ball-point-pen-32.png")), panel_Name, null);
		tabbedPane.setForegroundAt(1, Color.BLACK);
		tabbedPane.setBackgroundAt(1, new Color(255, 222, 173));
		GridBagLayout gbl_panel_Name = new GridBagLayout();
		gbl_panel_Name.columnWidths = new int[]{0, 0};
		gbl_panel_Name.rowHeights = new int[]{31, 0, 0, 33, 0, 24, 0, 0};
		gbl_panel_Name.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_Name.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_Name.setLayout(gbl_panel_Name);
		
		lblNameAct = new JLabel("Name of Activity:");
		lblNameAct.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNameAct = new GridBagConstraints();
		gbc_lblNameAct.insets = new Insets(0, 0, 5, 0);
		gbc_lblNameAct.gridx = 0;
		gbc_lblNameAct.gridy = 1;
		panel_Name.add(lblNameAct, gbc_lblNameAct);
		
		txtNameAct = new JTextField();
		txtNameAct.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtNameAct = new GridBagConstraints();
		gbc_txtNameAct.insets = new Insets(0, 0, 5, 0);
		gbc_txtNameAct.gridx = 0;
		gbc_txtNameAct.gridy = 2;
		panel_Name.add(txtNameAct, gbc_txtNameAct);
		txtNameAct.setColumns(10);
		
		btnSearchName = new JButton("Search");
		btnSearchName.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-search-24.png")));
		btnSearchName.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSearchName = new GridBagConstraints();
		gbc_btnSearchName.insets = new Insets(0, 0, 5, 0);
		gbc_btnSearchName.gridx = 0;
		gbc_btnSearchName.gridy = 4;
		panel_Name.add(btnSearchName, gbc_btnSearchName);
		
		btnEndSearchName = new JButton("End Search");
		btnEndSearchName.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-delete-24.png")));
		btnEndSearchName.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnEndSearchName = new GridBagConstraints();
		gbc_btnEndSearchName.gridx = 0;
		gbc_btnEndSearchName.gridy = 6;
		panel_Name.add(btnEndSearchName, gbc_btnEndSearchName);
		
		panel_Location = new JPanel();
		panel_Location.setForeground(Color.DARK_GRAY);
		panel_Location.setBackground(new Color(224, 255, 255));
		tabbedPane.addTab("Search LOCATION", new ImageIcon(frmMain.class.getResource("/resources/icons8-marker-32.png")), panel_Location, null);
		tabbedPane.setForegroundAt(2, Color.BLACK);
		tabbedPane.setBackgroundAt(2, new Color(255, 222, 173));
		GridBagLayout gbl_panel_Location = new GridBagLayout();
		gbl_panel_Location.columnWidths = new int[]{0, 0};
		gbl_panel_Location.rowHeights = new int[]{29, 0, 0, 24, 0, 19, 0, 0};
		gbl_panel_Location.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_Location.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_Location.setLayout(gbl_panel_Location);
		
		lblLocation = new JLabel("Location:");
		lblLocation.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblLocation = new GridBagConstraints();
		gbc_lblLocation.insets = new Insets(0, 0, 5, 0);
		gbc_lblLocation.gridx = 0;
		gbc_lblLocation.gridy = 1;
		panel_Location.add(lblLocation, gbc_lblLocation);
		
		txtLocation = new JTextField();
		txtLocation.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtLocation = new GridBagConstraints();
		gbc_txtLocation.insets = new Insets(0, 0, 5, 0);
		gbc_txtLocation.gridx = 0;
		gbc_txtLocation.gridy = 2;
		panel_Location.add(txtLocation, gbc_txtLocation);
		txtLocation.setColumns(10);
		
		btnSearchLocation = new JButton("Search");
		btnSearchLocation.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-search-24.png")));
		btnSearchLocation.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSearchLocation = new GridBagConstraints();
		gbc_btnSearchLocation.insets = new Insets(0, 0, 5, 0);
		gbc_btnSearchLocation.gridx = 0;
		gbc_btnSearchLocation.gridy = 4;
		panel_Location.add(btnSearchLocation, gbc_btnSearchLocation);
		
		btnEndSearchLocation = new JButton("End Search");
		btnEndSearchLocation.setIcon(new ImageIcon(frmMain.class.getResource("/resources/icons8-delete-24.png")));
		btnEndSearchLocation.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnEndSearchLocation = new GridBagConstraints();
		gbc_btnEndSearchLocation.gridx = 0;
		gbc_btnEndSearchLocation.gridy = 6;
		panel_Location.add(btnEndSearchLocation, gbc_btnEndSearchLocation);
	}
	public void setTableData(String[]header, Object[][]data) {
		tblActivity.setModel(new DefaultTableModel(data,header));
	}
	public ActivityRecord getSelectedActivity() {
		return (ActivityRecord) tblActivity.getValueAt(tblActivity.getSelectedRow(), 2);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (component instanceof JTable) {
					int red=((JTable) component).rowAtPoint(e.getPoint());
					if(red>=0 && red<((JTable) component).getRowCount()) {
						((JTable) component).setRowSelectionInterval(red, red);
					} else {
						((JTable) component).clearSelection();
					}
				if(e.isPopupTrigger() && ((JTable) component).getSelectedRow()>=0) {
					showMenu(e);
					}
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	public void EditActivity(ActionListener edit) {
		mntmEdit.addActionListener(edit);
	}
	public long getDateFrom() {
		long date1 =0;
		try {
			Date date = new SimpleDateFormat("dd.MM.yyyy").parse(txtDateFrom.getText());
			date1 = date.getTime()/1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date1;
	}
	public void setDateFrom(String string) {
			txtDateTo.setText(string);
	}
	public long getDateTo() {
		long date1 =0;
		try {
			Date date = new SimpleDateFormat("dd.MM.yyyy").parse(txtDateTo.getText());
			date1 = date.getTime()/1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date1;
	}
	public void setDateTo(String string) {
			txtDateTo.setText(string);
	}
	public String getNameAct () {
		return txtNameAct.getText();
	}
	public String getLocaticon() {
		return txtLocation.getText();
	}
	public void setSearchListenerDate(ActionListener search) {
		btnSearch.addActionListener(search);
	}
	public void setSearchListenerName(ActionListener search) {
		btnSearchName.addActionListener(search);
	}
	public void setSearchListenerLoc(ActionListener search) {
		btnSearchLocation.addActionListener(search);
	}
	public void setEndSearchDate(ActionListener search) {
		btnEndSearch.addActionListener(search);
	}
	public void setEndSearchName(ActionListener search) {
		btnEndSearchName.addActionListener(search);
	}
	public void setEndSearchLoc(ActionListener search) {
		btnEndSearchLocation.addActionListener(search);
	}
}

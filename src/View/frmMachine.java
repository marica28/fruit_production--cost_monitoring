package View;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class frmMachine extends JFrame {
	private JTable tblMachine;
	private JTextField txtNameMac;
	private JButton btnSave;

	
	public frmMachine(String arg0) {
		super(arg0);
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmMachine.class.getResource("/resources/icons8-engineering-100.png")));
		initialize();
	}

	private void initialize() {
	
		setBounds(50,50,443,405);
		JSplitPane splitPane = new JSplitPane();
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		tblMachine = new JTable();
		tblMachine.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblMachine.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblMachine);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setLeftComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 195, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 0, 5);
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 1;
		panel.add(lblName, gbc_lblName);
		
		txtNameMac = new JTextField();
		txtNameMac.setFont(new Font("Dialog", Font.PLAIN, 16));
		GridBagConstraints gbc_txtNameMac = new GridBagConstraints();
		gbc_txtNameMac.insets = new Insets(0, 0, 0, 5);
		gbc_txtNameMac.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNameMac.gridx = 1;
		gbc_txtNameMac.gridy = 1;
		panel.add(txtNameMac, gbc_txtNameMac);
		txtNameMac.setColumns(10);
		
		btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon(frmMachine.class.getResource("/resources/icons8-checked-24.png")));
		btnSave.setForeground(SystemColor.desktop);
		btnSave.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.gridx = 2;
		gbc_btnSave.gridy = 1;
		panel.add(btnSave, gbc_btnSave);
	}

	public String getName() {
		return txtNameMac.getText();
	}
	public void setName(String name) {
		txtNameMac.setText(name);
	}
	public void setSaveListener(ActionListener save) {
		btnSave.addActionListener(save);
	}
	public void setTableData(String[] header, Object[][] data) {
		tblMachine.setModel(new DefaultTableModel(data,header));
	}
	public void ClearF() {
		txtNameMac.setText("");
	}

}

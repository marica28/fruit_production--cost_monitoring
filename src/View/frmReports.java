package View;

import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JSplitPane;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class frmReports extends JFrame {
	private JTable tblMachineReport;
	private JTable tblMaterialReport;
	private JTable tblWorkerReport;
	private JLabel lblSumMat;
	private JLabel lblSumMac;
	private JLabel lblSumWor;
	private JButton btnPDFMachine;
	private JButton btnPdfM;
	private JButton btnPdfW;


	public frmReports(String title) throws HeadlessException {
		super(title);
		setIconImage(Toolkit.getDefaultToolkit().getImage(frmReports.class.getResource("/resources/iconfinder_business-work_12_2377635.png")));
		initialize();
	
	}
	
	private void initialize() {
		setBounds(50,50,653,505);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Dialog", Font.BOLD, 18));
		tabbedPane.setBackground(new Color(255, 222, 173));
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.95);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		tabbedPane.addTab("Machines", new ImageIcon(frmReports.class.getResource("/resources/icons8-services-32.png")), splitPane, null);
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setLeftComponent(scrollPane);
		
		tblMachineReport = new JTable();
		tblMachineReport.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblMachineReport.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblMachineReport);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(224, 255, 255));
		splitPane.setRightComponent(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{458, 100, 0};
		gbl_panel.rowHeights = new int[]{0, 14, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblSumCost = new JLabel("Sum:");
		lblSumCost.setFont(new Font("Dialog", Font.BOLD, 16));
		GridBagConstraints gbc_lblSumCost = new GridBagConstraints();
		gbc_lblSumCost.anchor = GridBagConstraints.WEST;
		gbc_lblSumCost.insets = new Insets(0, 0, 5, 5);
		gbc_lblSumCost.fill = GridBagConstraints.VERTICAL;
		gbc_lblSumCost.gridx = 0;
		gbc_lblSumCost.gridy = 0;
		panel.add(lblSumCost, gbc_lblSumCost);
		
		lblSumMac = new JLabel("SumMachine");
		lblSumMac.setFont(new Font("Dialog", Font.BOLD, 16));
		lblSumMac.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblSumMac = new GridBagConstraints();
		gbc_lblSumMac.insets = new Insets(0, 0, 5, 0);
		gbc_lblSumMac.anchor = GridBagConstraints.EAST;
		gbc_lblSumMac.fill = GridBagConstraints.VERTICAL;
		gbc_lblSumMac.gridx = 1;
		gbc_lblSumMac.gridy = 0;
		panel.add(lblSumMac, gbc_lblSumMac);
		
		btnPDFMachine = new JButton("Export PDF");
		btnPDFMachine.setIcon(new ImageIcon(frmReports.class.getResource("/resources/icons8-pdf-24.png")));
		btnPDFMachine.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnPDFMachine = new GridBagConstraints();
		gbc_btnPDFMachine.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPDFMachine.gridx = 1;
		gbc_btnPDFMachine.gridy = 1;
		panel.add(btnPDFMachine, gbc_btnPDFMachine);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.95);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		tabbedPane.addTab("Materials", new ImageIcon(frmReports.class.getResource("/resources/icons8-sales-performance-32.png")), splitPane_1, null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		splitPane_1.setLeftComponent(scrollPane_2);
		
		tblMaterialReport = new JTable();
		tblMaterialReport.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblMaterialReport.setFillsViewportHeight(true);
		scrollPane_2.setViewportView(tblMaterialReport);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(224, 255, 255));
		splitPane_1.setRightComponent(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{458, 100, 0};
		gbl_panel_2.rowHeights = new int[]{0, 15, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel lblSumCost1 = new JLabel("Sum:");
		lblSumCost1.setFont(new Font("Dialog", Font.BOLD, 16));
		GridBagConstraints gbc_lblSumCost1 = new GridBagConstraints();
		gbc_lblSumCost1.anchor = GridBagConstraints.WEST;
		gbc_lblSumCost1.insets = new Insets(0, 0, 5, 5);
		gbc_lblSumCost1.gridx = 0;
		gbc_lblSumCost1.gridy = 0;
		panel_2.add(lblSumCost1, gbc_lblSumCost1);
		
		lblSumMat = new JLabel("SumMaterial");
		lblSumMat.setFont(new Font("Dialog", Font.BOLD, 16));
		lblSumMat.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblSumMat = new GridBagConstraints();
		gbc_lblSumMat.insets = new Insets(0, 0, 5, 0);
		gbc_lblSumMat.anchor = GridBagConstraints.EAST;
		gbc_lblSumMat.fill = GridBagConstraints.VERTICAL;
		gbc_lblSumMat.gridx = 1;
		gbc_lblSumMat.gridy = 0;
		panel_2.add(lblSumMat, gbc_lblSumMat);
		
		btnPdfM = new JButton("Export PDF");
		btnPdfM.setIcon(new ImageIcon(frmReports.class.getResource("/resources/icons8-pdf-24.png")));
		btnPdfM.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnPdfM = new GridBagConstraints();
		gbc_btnPdfM.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPdfM.gridx = 1;
		gbc_btnPdfM.gridy = 1;
		panel_2.add(btnPdfM, gbc_btnPdfM);
		
		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setBackground(new Color(255, 255, 255));
		splitPane_2.setResizeWeight(0.95);
		splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);
		tabbedPane.addTab("Workers", new ImageIcon(frmReports.class.getResource("/resources/icons8-workers-32.png")), splitPane_2, null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		splitPane_2.setLeftComponent(scrollPane_1);
		
		tblWorkerReport = new JTable();
		tblWorkerReport.setFont(new Font("Dialog", Font.PLAIN, 16));
		tblWorkerReport.setFillsViewportHeight(true);
		scrollPane_1.setViewportView(tblWorkerReport);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(224, 255, 255));
		splitPane_2.setRightComponent(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{450, 109, 0};
		gbl_panel_1.rowHeights = new int[]{15, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblSumCost2 = new JLabel("Sum:");
		lblSumCost2.setFont(new Font("Dialog", Font.BOLD, 16));
		GridBagConstraints gbc_lblSumCost2 = new GridBagConstraints();
		gbc_lblSumCost2.anchor = GridBagConstraints.WEST;
		gbc_lblSumCost2.insets = new Insets(0, 0, 5, 5);
		gbc_lblSumCost2.gridx = 0;
		gbc_lblSumCost2.gridy = 0;
		panel_1.add(lblSumCost2, gbc_lblSumCost2);
		
		lblSumWor = new JLabel("SumWorker");
		lblSumWor.setFont(new Font("Dialog", Font.BOLD, 16));
		lblSumWor.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblSumWor = new GridBagConstraints();
		gbc_lblSumWor.insets = new Insets(0, 0, 5, 0);
		gbc_lblSumWor.anchor = GridBagConstraints.EAST;
		gbc_lblSumWor.fill = GridBagConstraints.VERTICAL;
		gbc_lblSumWor.gridx = 1;
		gbc_lblSumWor.gridy = 0;
		panel_1.add(lblSumWor, gbc_lblSumWor);
		
		btnPdfW = new JButton("Export PDF");
		btnPdfW.setIcon(new ImageIcon(frmReports.class.getResource("/resources/icons8-pdf-24.png")));
		btnPdfW.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 17));
		GridBagConstraints gbc_btnPdfW = new GridBagConstraints();
		gbc_btnPdfW.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPdfW.gridx = 1;
		gbc_btnPdfW.gridy = 1;
		panel_1.add(btnPdfW, gbc_btnPdfW);
	}
	
	public void setTableDataMas(String [] header, Object[][] data) {
		tblMachineReport.setModel(new DefaultTableModel(data,header));
	}
	public void setTableDataMat(String [] header1, Object[][] data1) {
		tblMaterialReport.setModel(new DefaultTableModel(data1,header1));
	}
	public void setTableDataRad(String [] header2, Object[][] data2) {
		tblWorkerReport.setModel(new DefaultTableModel(data2,header2));
	}
	public void setSumMac(Double sum) {
		lblSumMac.setText(Double.toString(sum));
	}
	public void setSumMat(Double sum) {
		lblSumMat.setText(Double.toString(sum));
	}
	public void setSumWor(Double sum) {
		lblSumWor.setText(Double.toString(sum));
	}
	public void PDFMachine(ActionListener mac) {
		btnPDFMachine.addActionListener(mac);
	}
	public void PDFMaterial(ActionListener mat) {
		btnPdfM.addActionListener(mat);
	}
	public void PDFWorker(ActionListener wor) {
		btnPdfW.addActionListener(wor);
	}
}

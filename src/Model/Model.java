package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Observable;

import Data.ActivityRecord;
import Data.Machine;
import Data.MachineItem;
import Data.Material;
import Data.MaterialItem;
import Data.Worker;
import Data.WorkerItem;
import Data.TypeOfPayment;

public class Model extends Observable{
/////////////////////////////////////////////////////////////////////getAll
	public void getAllActivities() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<ActivityRecord>activity = new ArrayList<ActivityRecord>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM activity_record;");
				while (rs.next())
				{
					ActivityRecord act = new ActivityRecord(rs.getInt(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getString(5));
					activity.add(act);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(activity);
	}
	public void getAllWorker() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<Worker>workers = new ArrayList<Worker>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM worker;");
				while (rs.next())
				{
					Worker rd = new Worker(rs.getInt(1), rs.getString(2), rs.getString(3), getTip(rs.getInt(4)));
					workers.add(rd);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(workers);
	}
	public void getAllPayment() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<TypeOfPayment>payment = new ArrayList<TypeOfPayment>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM type_payment;");
				while (rs.next())
				{
					TypeOfPayment type = new TypeOfPayment(rs.getInt(1), rs.getString(2));
					payment.add(type);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(payment);
	}
	public void getAllMaterial() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<Material>material = new ArrayList<Material>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM material;");
				while (rs.next())
				{
					Material mr = new Material(rs.getInt(1), rs.getString(2), rs.getString(3));
					material.add(mr);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(material);
	}
	public void getAllMachine() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<Machine>machine = new ArrayList<Machine>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM machine;");
				while (rs.next())
				{
					Machine mr = new Machine(rs.getInt(1), rs.getString(2));
					machine.add(mr);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(machine);
	}
	public void getAllMachineItems() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<MachineItem>machine = new ArrayList<MachineItem>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM cost_monitoring.machine_item;");
				while (rs.next())
				{
					MachineItem mr = new MachineItem(rs.getInt(1), getMasinaId(rs.getInt(2)), rs.getFloat(3), rs.getFloat(4), getAktivnostId(rs.getInt(5)));
					machine.add(mr);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(machine);
	}
	public void getAllMaterialItems() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<MaterialItem>material = new ArrayList<MaterialItem>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM cost_monitoring.material_item;");
				while (rs.next())
				{
					MaterialItem mat = new MaterialItem(rs.getInt(1), getMaterijalId(rs.getInt(2)), rs.getFloat(3), rs.getFloat(4), getAktivnostId(rs.getInt(5)));
					material.add(mat);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(material);
	}
	public void getAllWorkerItems() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<WorkerItem>workers = new ArrayList<WorkerItem>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM cost_monitoring.worker_item;");
				while (rs.next())
				{
					WorkerItem rad = new WorkerItem(rs.getInt(1), getRadnikId(rs.getInt(2)), rs.getFloat(3), rs.getFloat(4), getAktivnostId(rs.getInt(5)));
					workers.add(rad);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(workers);
	}
//////////////////////////////////////////////////////////////////////////////////////////////getId
	public TypeOfPayment getTip(int id)
	{
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		TypeOfPayment tip = null;
		try {
			Statement upit = connection.createStatement();
			ResultSet tip2 = upit.executeQuery("Select * from type_payment where type_of_payment_id="+id);
			while (tip2.next())
			{
				tip = new TypeOfPayment(tip2.getInt(1),tip2.getString(2));
			}
			tip2.close();
			upit.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				connection.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return tip;
	}
	public ActivityRecord getAktivnostId(int id)
	{
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		ActivityRecord tip = null;
		try {
			Statement upit = connection.createStatement();
			ResultSet tip2 = upit.executeQuery("Select * from activity_record where act_id="+id);
			while (tip2.next())
			{
				tip = new ActivityRecord(tip2.getInt(1), tip2.getLong(2), tip2.getString(3), tip2.getString(4), tip2.getString(5));
			}
			tip2.close();
			upit.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				connection.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return tip;
	}
	public Machine getMasinaId(int id)
	{
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		Machine tip = null;
		try {
			Statement upit = connection.createStatement();
			ResultSet tip2 = upit.executeQuery("Select * from machine where machine_id="+id);
			while (tip2.next())
			{
				tip = new Machine(tip2.getInt(1), tip2.getString(2));
			}
			tip2.close();
			upit.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				connection.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return tip;
	}
	public Material getMaterijalId(int id)
	{
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		Material tip = null;
		try {
			Statement upit = connection.createStatement();
			ResultSet tip2 = upit.executeQuery("Select * from material where material_id="+id);
			while (tip2.next())
			{
				tip = new Material(tip2.getInt(1), tip2.getString(2), tip2.getString(3));
			}
			tip2.close();
			upit.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				connection.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return tip;
	}
	public Worker getRadnikId(int id)
	{
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		Worker tip = null;
		try {
			Statement upit = connection.createStatement();
			ResultSet tip2 = upit.executeQuery("Select * from worker where worker_id="+id);
			while (tip2.next())
			{
				tip = new Worker(tip2.getInt(1), tip2.getString(2), tip2.getString(3), getTip(tip2.getInt(4)));
			}
			tip2.close();
			upit.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				connection.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return tip;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////save
	public void SaveWorker(Worker worker) {
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO worker(nameWor,phone_number,payment) VALUES (?,?,?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setString(1,  worker.getNameWor());
			upit.setString(2, worker.getPhone_number());
			upit.setInt(3, worker.getPayment().getType_of_payment_id());
			upit.executeUpdate();
		} catch (SQLException e)
		{
			e.printStackTrace();
					
		}
	}
	
	public void SavePayment(TypeOfPayment payment) {
		MySqlConnection conn=new MySqlConnection();
		Connection connection = conn.getConnection();
		try 
		{
			String query = "INSERT INTO type_payment(name) VALUES (?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setString(1, payment.getName());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void SaveMaterial(Material material) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO material(nameMat, unit_measure) VALUES (?,?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setString(1, material.getNameMat());
			upit.setString(2, material.getUnit_measure());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void SaveMachine(Machine machine) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO machine(nameMac) VALUES (?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setString(1, machine.getNameMac());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void SaveActivity(ActivityRecord activity) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO activity_record(dateAct,nameAct,descriptionAct,locationAct) VALUES (?,?,?,?)";
			PreparedStatement upit = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			upit.setLong(1, activity.getDateAct());
			upit.setString(2, activity.getNameAct());
			upit.setString(3, activity.getDescriptionAct());
			upit.setString(4, activity.getLocationAct());
			int id = upit.executeUpdate();
			ResultSet rs = upit.getGeneratedKeys();
			if(rs.next()){
				id = rs.getInt(1);
			}
			activity.setAct_id(id);
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void SaveMachineItem(MachineItem machineItem) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO cost_monitoring.machine_item(machine,work_time_machine,labor_cost_machine,activity_id) VALUES (?,?,?,?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setInt(1, machineItem.getMachine().getMachine_id());
			upit.setFloat(2, machineItem.getWork_time_machine());
			upit.setFloat(3, machineItem.getLabor_cost_machine());
			upit.setInt(4, machineItem.getActivity().getAct_id());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void EditMachineItem(MachineItem machineItem) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "UPDATE cost_monitoring.machine_item SET machine=?, work_time_machine=?, labor_cost_machine=?, activity_id=? WHERE (machine_item_id = "+machineItem.getMachine_item_id()+")";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setInt(1,  machineItem.getMachine().getMachine_id());
			upit.setFloat(2, machineItem.getWork_time_machine());
			upit.setFloat(3, machineItem.getLabor_cost_machine());
			upit.setInt(4, machineItem.getActivity().getAct_id());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void SaveMaterialItem(MaterialItem materialItem) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO cost_monitoring.material_item(material, quantity_material, cost_material, activity_id) VALUES (?,?,?,?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setInt(1, materialItem.getMaterial().getMaterial_id());
			upit.setFloat(2, materialItem.getQuantity_material());
			upit.setFloat(3, materialItem.getCost_material());
			upit.setInt(4, materialItem.getActivity().getAct_id());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void EditMaterialItem(MaterialItem materialItem) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "UPDATE cost_monitoring.material_item SET material=?, quantity_material=?, cost_material=?, activity_id=? WHERE (material_item_id = "+materialItem.getMaterial_item_id()+")" ;
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setInt(1, materialItem.getMaterial().getMaterial_id());
			upit.setFloat(2,materialItem.getQuantity_material());
			upit.setFloat(3, materialItem.getCost_material());
			upit.setInt(4, materialItem.getActivity().getAct_id());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void SaveWorkerItem(WorkerItem workerItem) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query = "INSERT INTO cost_monitoring.worker_item(worker, work_time_worker, cost, activity_id) VALUES (?,?,?,?)";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setInt(1, workerItem.getWorker().getWorker_id());
			upit.setFloat(2, workerItem.getWork_time_worker());
			upit.setFloat(3, workerItem.getCost());
			upit.setInt(4, workerItem.getActivity().getAct_id());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void EditWorkerItem(WorkerItem workerItem) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try
		{
			String query =  "UPDATE cost_monitoring.worker_item SET worker=?,work_time_worker=?,cost=?, activity_id=? WHERE (worker_item_id = "+workerItem.getWorker_item_id()+")" ;
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setInt(1, workerItem.getWorker().getWorker_id());
			upit.setFloat(2, workerItem.getWork_time_worker());
			upit.setFloat(3, workerItem.getCost());
			upit.setInt(4, workerItem.getActivity().getAct_id());
			upit.executeUpdate();
		} catch(SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void EditActivity(ActivityRecord activity) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		try {
			String query = "UPDATE cost_monitoring.activity_record SET dateAct=?, nameAct=?, descriptionAct=?, locationAct=? WHERE (act_id = "+activity.getAct_id()+")";
			PreparedStatement upit = connection.prepareStatement(query);
			upit.setLong(1, activity.getDateAct());
			upit.setString(2, activity.getNameAct());
			upit.setString(3, activity.getDescriptionAct());
			upit.setString(4, activity.getLocationAct());
			upit.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////reports
	public void getAllWorkerReport() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<WorkerItem>item = new ArrayList<WorkerItem>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT worker, sum(work_time_worker), sum(work_time_worker*cost) FROM cost_monitoring.worker_item rs left join worker r on rs.worker = r.worker_id group by worker;");
				while (rs.next())
				{
					WorkerItem rd = new WorkerItem(getRadnikId(rs.getInt(1)), rs.getFloat(2), rs.getFloat(3));
					item.add(rd);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(item);
	}
	public void getAllMachineReport() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<MachineItem>item = new ArrayList<MachineItem>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT machine, sum(work_time_machine), sum(work_time_machine*labor_cost_machine) FROM cost_monitoring.machine_item ms left join machine m on ms.machine= m.machine_id group by machine;");
				while (rs.next())
				{
					MachineItem md = new MachineItem(getMasinaId(rs.getInt(1)), rs.getFloat(2), rs.getFloat(3));
					item.add(md);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(item);
	}
	public void getAllMaterialReport() {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<MaterialItem>item = new ArrayList<MaterialItem>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT material, sum(quantity_material), sum(cost_material*quantity_material) FROM cost_monitoring.material_item ms left join material m on ms.material = m.material_id group by material");
				while (rs.next())
				{
					MaterialItem ms = new MaterialItem(getMaterijalId(rs.getInt(1)), rs.getFloat(2), rs.getFloat(3));
					item.add(ms);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(item);
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////SEARCH	
	public void getActivityDate(Long date1, Long date2) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<ActivityRecord>activity = new ArrayList<ActivityRecord>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM cost_monitoring.activity_record WHERE dateAct BETWEEN " + date1 +" AND "+ date2);
				while (rs.next())
				{
					ActivityRecord ea = new ActivityRecord(rs.getInt(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getString(5));
					activity.add(ea);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(activity);
	}
	public void getActivityLocation(String location) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<ActivityRecord>activity = new ArrayList<ActivityRecord>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM cost_monitoring.activity_record WHERE locationAct = '"+location+"'");
				while (rs.next())
				{
					ActivityRecord ea = new ActivityRecord(rs.getInt(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getString(5));
					activity.add(ea);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(activity);
	}
	public void getActivityName(String name) {
		MySqlConnection conn = new MySqlConnection();
		Connection connection = conn.getConnection();
		ArrayList<ActivityRecord>activity = new ArrayList<ActivityRecord>();
		try
			{
				Statement upit = connection.createStatement();
				ResultSet rs = upit.executeQuery("SELECT * FROM cost_monitoring.activity_record WHERE nameAct = '" +name+"'");
				while (rs.next())
				{
					ActivityRecord ea = new ActivityRecord(rs.getInt(1), rs.getLong(2), rs.getString(3), rs.getString(4), rs.getString(5));
					activity.add(ea);
				}
			}catch (SQLException e)
			{
				e.printStackTrace();
			}
		setChanged();
		notifyObservers(activity);
	}
}


import Controllers.MainCtrl;
import Model.Model;
import View.frmMain;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
	
		frmMain view = new frmMain("Fruit Production - Management of Activities and Costs");
		Model model = new Model();
		MainCtrl con = new MainCtrl(view, model);
		view.setVisible(true);
	}

}

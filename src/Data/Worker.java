package Data;

public class Worker {

	private int worker_id;
	private String nameWor;
	private String phone_number;
	private TypeOfPayment payment;
	
	
	public int getWorker_id() {
		return worker_id;
	}
	public void setWorker_id(int worker_id) {
		this.worker_id = worker_id;
	}
	public String getNameWor() {
		return nameWor;
	}
	public void setNameWor(String nameWor) {
		this.nameWor = nameWor;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public TypeOfPayment getPayment() {
		return payment;
	}
	public void setPayment(TypeOfPayment payment) {
		this.payment = payment;
	}
	
	public Worker(int worker_id, String nameWor, String phone_number, TypeOfPayment payment) {
		super();
		this.worker_id = worker_id;
		this.nameWor = nameWor;
		this.phone_number = phone_number;
		this.payment = payment;
	}

	@Override
	public String toString() {
		return nameWor+" ("+payment+")";
	}
	
	
}

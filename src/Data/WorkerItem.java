package Data;

public class WorkerItem {

	private int worker_item_id;
	private ActivityRecord activity;
	private Worker worker;
	private float cost;
	private float work_time_worker;
	

	public int getWorker_item_id() {
		return worker_item_id;
	}
	public void setWorker_item_id(int worker_item_id) {
		this.worker_item_id = worker_item_id;
	}
	public ActivityRecord getActivity() {
		return activity;
	}
	public void setActivity(ActivityRecord activity) {
		this.activity = activity;
	}
	public Worker getWorker() {
		return worker;
	}
	public void setWorker(Worker worker) {
		this.worker = worker;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public float getWork_time_worker() {
		return work_time_worker;
	}
	public void setWork_time_worker(float work_time_worker) {
		this.work_time_worker = work_time_worker;
	}
	
	public WorkerItem(int worker_item_id, Worker worker, float work_time_worker, float cost, ActivityRecord activity) {
		super();
		this.worker_item_id = worker_item_id;
		this.activity = activity;
		this.worker = worker;
		this.work_time_worker = work_time_worker;
		this.cost = cost;
	
	}
	
	public WorkerItem(Worker worker, float work_time_worker, float cost) {
		super();
		this.worker = worker;
		this.work_time_worker = work_time_worker;
		this.cost = cost;
	
	}
	@Override
	public String toString() {
		return worker.getNameWor()+" ("+worker.getPayment()+")";
	}
	
	
	
}

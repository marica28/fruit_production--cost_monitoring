package Data;

public class Machine {

	private int machine_id;
	private String nameMac;
	
	
	public int getMachine_id() {
		return machine_id;
	}

	public void setMachine_id(int machine_id) {
		this.machine_id = machine_id;
	}

	public String getNameMac() {
		return nameMac;
	}

	public void setNameMac(String nameMac) {
		this.nameMac = nameMac;
	}

	public Machine(int machine_id, String nameMac) {
		super();
		this.machine_id = machine_id;
		this.nameMac = nameMac;
	}

	@Override
	public String toString() {
		return nameMac;
	}
	
	
}

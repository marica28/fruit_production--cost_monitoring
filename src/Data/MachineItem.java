package Data;

public class MachineItem {

	private int machine_item_id;
	private ActivityRecord activity;
	private Machine machine;
	private float work_time_machine;
	private float labor_cost_machine;
	private String action;
	
	public int getMachine_item_id() {
		return machine_item_id;
	}
	public void setMachine_item_id(int machine_item_id) {
		this.machine_item_id = machine_item_id;
	}
	public ActivityRecord getActivity() {
		return activity;
	}
	public void setActivity(ActivityRecord activity) {
		this.activity = activity;
	}
	public Machine getMachine() {
		return machine;
	}
	public void setMachine(Machine machine) {
		this.machine = machine;
	}
	public float getWork_time_machine() {
		return work_time_machine;
	}
	public void setWork_time_machine(float work_time_machine) {
		this.work_time_machine = work_time_machine;
	}
	public float getLabor_cost_machine() {
		return labor_cost_machine;
	}
	public void setLabor_cost_machine(float labor_cost_machine) {
		this.labor_cost_machine = labor_cost_machine;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	
	}
	public MachineItem(Machine machine, float work_time_machine, float labor_cost_machine) {
		this.machine = machine;
		this.work_time_machine = work_time_machine;
		this.labor_cost_machine = labor_cost_machine;
	}
	
	public MachineItem(int machine_item_id, Machine machine, float work_time_machine,
			float labor_cost_machine, ActivityRecord activity) {
		super();
		this.machine_item_id = machine_item_id;
		this.activity = activity;
		this.machine = machine;
		this.work_time_machine = work_time_machine;
		this.labor_cost_machine = labor_cost_machine;
	}
	
	public String toString() {
		return machine.getNameMac();
	}
}

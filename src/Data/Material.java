package Data;

public class Material {

	private int material_id;
	private String nameMat;
	private String unit_measure;
	
	
	
	public int getMaterial_id() {
		return material_id;
	}

	public void setMaterial_id(int material_id) {
		this.material_id = material_id;
	}
	
	public String getNameMat() {
		return nameMat;
	}

	public void setNameMat(String nameMat) {
		this.nameMat = nameMat;
	}

	public String getUnit_measure() {
		return unit_measure;
	}

	public void setUnit_measure(String unit_measure) {
		this.unit_measure = unit_measure;
	}

	public Material(int material_id, String nameMat, String unit_measure) {
		super();
		this.material_id = material_id;
		this.nameMat = nameMat;
		this.unit_measure = unit_measure;
	}

	@Override
	public String toString() {
		return nameMat+" ("+unit_measure+")";
	}

	
	
}

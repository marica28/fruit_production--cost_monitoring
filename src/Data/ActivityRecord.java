package Data;

public class ActivityRecord {

	private int act_id;
	private long dateAct;
	private String nameAct;
	private String descriptionAct;
	private String locationAct;
	
	public int getAct_id() {
		return act_id;
	}
	public void setAct_id(int act_id) {
		this.act_id = act_id;
	}
	public long getDateAct() {
		return dateAct;
	}
	public void setDateAct(long dateAct) {
		this.dateAct = dateAct;
	}
	public String getNameAct() {
		return nameAct;
	}
	public void setNameAct(String nameAct) {
		this.nameAct = nameAct;
	}
	public String getDescriptionAct() {
		return descriptionAct;
	}
	public void setDescriptionAct(String descriptionAct) {
		this.descriptionAct = descriptionAct;
	}
	public String getLocationAct() {
		return locationAct;
	}
	public void setLocationAct(String locationAct) {
		this.locationAct = locationAct;
	}
	
	public ActivityRecord(int act_id, long dateAct, String nameAct, String descriptionAct, String locationAct) {
		super();
		this.act_id = act_id;
		this.dateAct = dateAct;
		this.nameAct = nameAct;
		this.descriptionAct = descriptionAct;
		this.locationAct = locationAct;
	}
	@Override
	public String toString() {
		return nameAct;
	}
	
	
	
}

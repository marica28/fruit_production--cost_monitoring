package Data;

public class TypeOfPayment {

	private int type_of_payment_id;
	private String name;
	
	
	public int getType_of_payment_id() {
		return type_of_payment_id;
	}
	
	public void setType_of_payment_id(int type_of_payment_id) {
		this.type_of_payment_id = type_of_payment_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public TypeOfPayment(int type_of_payment_id, String name) {
		super();
		this.type_of_payment_id = type_of_payment_id;
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
}

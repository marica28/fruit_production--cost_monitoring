package Data;

public class MaterialItem{

	private int material_item_id;
	private ActivityRecord activity;
	private Material material;
	private float quantity_material;
	private float cost_material;

	
	public int getMaterial_item_id() {
		return material_item_id;
	}
	public void setMaterial_item_id(int material_item_id) {
		this.material_item_id = material_item_id;
	}
	public ActivityRecord getActivity() {
		return activity;
	}
	public void setActivity(ActivityRecord activity) {
		this.activity = activity;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public float getQuantity_material() {
		return quantity_material;
	}
	public void setQuantity_material(float quantity_material) {
		this.quantity_material = quantity_material;
	}
	public float getCost_material() {
		return cost_material;
	}
	public void setCost_material(float cost_material) {
		this.cost_material = cost_material;
	}
	
	public MaterialItem(int material_item_id, Material material, float quantity_material,
			float cost_material, ActivityRecord activity) {
		super();
		this.material_item_id = material_item_id;
		this.activity = activity;
		this.material = material;
		this.quantity_material = quantity_material;
		this.cost_material = cost_material;
	}
	
	public MaterialItem(Material material, float quantity_material, float cost_material) {
		super();
		this.material = material;
		this.quantity_material = quantity_material;
		this.cost_material = cost_material;
	}

	@Override
	public String toString() {
		return material.getNameMat();
	}
	
	
	
	
}
